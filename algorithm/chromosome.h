#ifndef CHROMOSOME_H
#define CHROMOSOME_H

class Function;
class Chromosome
{
public:
    explicit Chromosome();
    ~Chromosome();

    Function* root() { return m_root; }
    void setRoot(Function* root) { m_root = root; }


private:
    Function* m_root;
};

#endif // CHROMOSOME_H
