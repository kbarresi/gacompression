#include "errorimagefactory.h"
#include "../algorithm/program/function.h"


#include <QDebug>
#include <QImage>

ErrorImageFactory::ErrorImageFactory(QImage &image, Function *red, Function *green, Function* blue)
{
    m_inputImage = image;
    m_redPrediction = red;
    m_greenPrediction = green;
    m_bluePrediction = blue;
}
ErrorImageFactory::~ErrorImageFactory() {
    //we don't own the Function*, so leave it
}

QVector<QVector<int> > ErrorImageFactory::redImage() {
    return getImage(qRed);
}
QVector<QVector<int> > ErrorImageFactory::greenImage() {
    return getImage(qGreen);
}
QVector<QVector<int> > ErrorImageFactory::blueImage() {
    return getImage(qBlue);
}

QVector<QVector<int> > ErrorImageFactory::getImage(PixelGrabber pixelGrabber) {
    QVector<QVector<int> > outputArray;
    for (int i = 0; i < m_inputImage.height(); i++) {
        QVector<int> list;
        list.fill(0, m_inputImage.width());

        outputArray.append(list);
    }



    QRgb* tailingPtr = 0;   //stays 1 row behind
    QRgb* colorPtr = 0;     //current row


    for (int y = 0; y < m_inputImage.height(); y++) {
        colorPtr = (QRgb*) m_inputImage.scanLine(y);
        tailingPtr = (QRgb*) m_inputImage.scanLine(y - 1);

        for (int x = 0; x < m_inputImage.width(); x++) {

            if (y == 0 || x == 0) {   //just copy the pixel if it's a border pixel
                const uchar currentPixel = pixelGrabber(*((QRgb*) colorPtr));


                (outputArray.data()[y].replace(x, currentPixel));

                colorPtr++;
                tailingPtr++;

                continue;
            }

            const int currentPixel = pixelGrabber(*((QRgb*) colorPtr));;


            pixNeighbor_t neighbors;
            neighbors.left = pixelGrabber(*((QRgb*) colorPtr - 1));
            neighbors.topLeft = pixelGrabber(*((QRgb*) tailingPtr - 1));
            neighbors.top = pixelGrabber((*(QRgb*) tailingPtr));
            neighbors.topRight = pixelGrabber((*(QRgb*) tailingPtr + 1));

            const int predictedPixel = m_redPrediction->compute(neighbors);

            int diff = currentPixel - predictedPixel;
            (outputArray.data()[y]).replace(x, diff);

            colorPtr++;
            tailingPtr++;
        }
    }

    return outputArray;
}

