#include "node.h"

#include "function.h"
#include "terminal.h"

Node::Node(Function* parent)
{
    m_parent = parent;

    if (parent) {
        if (!parent->childOne())
            parent->setChildOne(this);
        else
            parent->setChildTwo(this);
    }

    m_id = 0;
}
