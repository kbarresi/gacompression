#include "function.h"

#include "terminal.h"

#include <QStack>
#include <QDebug>
#include <QStringList>
#include <QPair>

Function::Function(FunctionType type, Function* parent)
    : Node(parent)
{
    m_type = type;
    m_nodeType = Node::FunctionType;

    m_childOne = 0;
    m_childTwo = 0;
}

Function::~Function() {
    if (m_childOne)
        delete m_childOne;

    if (m_childTwo)
        delete m_childTwo;
}

void Function::setChildOne(Node *child) {
    if (m_childOne) //if we already have a child one, make sure to update it's parent (0)
        m_childOne->setParentNode(0);

    m_childOne = child;
    m_childOne->setParentNode(this);
}
void Function::setChildTwo(Node *child) {
    if (m_childTwo)
        m_childTwo->setParentNode(0);

    m_childTwo = child;
    m_childTwo->setParentNode(this);
}

QString Function::expression(const pixNeighbor_t neighborhood) {
    QString expressionString = "";
    QString childOneExpression = "";
    QString childTwoExpression = "";

    if (m_childOne->nodeType() == Node::FunctionType) {   //this child is a function, recurse and get this function's exp str
        Function* childOne = static_cast<Function*>(m_childOne);
        childOneExpression = childOne->expression(neighborhood);
        childOneExpression.prepend('(');
        childOneExpression.append(')');
    } else {
        Terminal* childOne = static_cast<Terminal*>(m_childOne);
        childOneExpression = childOne->terminalString(neighborhood);
    }

    //now get the expression for child two
    if (m_childTwo->nodeType() == Node::FunctionType) {
        Function* childTwo = static_cast<Function*>(m_childTwo);
        childTwoExpression = childTwo->expression(neighborhood);
        childTwoExpression.prepend('(');
        childTwoExpression.append(')');
    } else {
        Terminal* childTwo = static_cast<Terminal*>(m_childTwo);
        childTwoExpression = childTwo->terminalString(neighborhood);
    }

    //got the expressions for both children. Now, we need to insert our function operator between the two children

    QString operationExpression = functionString();

    expressionString = childOneExpression + operationExpression + childTwoExpression;

    return expressionString;
}

QList<Node*> Function::getChildren() {
    QList<Node*> childrenList;

    childrenList.append(this);  //first add the current node (this)

    //get the first kids stuff
    if (m_childOne) {
        if (m_childOne->nodeType() == Node::FunctionType) {
            Function *childOne = static_cast<Function*>(m_childOne);
            childrenList.append(childOne->getChildren());
        } else {
            childrenList.append(m_childOne);  //just add the terminal
        }
    }

    if (m_childTwo) {
        if (m_childTwo->nodeType() == Node::FunctionType) {
            Function* childTwo =  static_cast<Function*>(m_childTwo);
            childrenList.append(childTwo->getChildren());
        } else {
            childrenList.append(m_childTwo);
        }
    }

    return childrenList;
}


QString Function::functionString() {
    switch (m_type) {
    case Add:
        return "+";
    case Subtract:
        return "-";
    case Multiply:
        return "*";
    case Divide:
        return "/";
    case Min:
        return "min";
    case Max:
        return "max";
    default:
        return "(ERROR)";
    }
}

Function::OperationFunction Function::functionPointer() {
    switch (m_type) {
    case Add:
        return &Node::add;
    case Subtract:
        return &Node::sub;
    case Multiply:
        return &Node::mul;
    case Divide:
        return &Node::div;
    case Min:
        return &Node::min;
    case Max:
        return &Node::max;
    default:
        return &Node::add;
    }
}

Function* Function::deepCopy(Function* parent) {
    Function* current = new Function(functionType(), parent);

    if (m_childOne) {
        if (m_childOne->nodeType() == Node::FunctionType)
            current->setChildOne(((Function*)m_childOne)->deepCopy(current));
        else {
            Terminal* currentChildOne = (Terminal*) m_childOne;
            Terminal* newChildOne = new Terminal(currentChildOne->terminalType(), current);
            Q_UNUSED(newChildOne);
        }
    }

    if (m_childTwo) {
        if (m_childTwo->nodeType() == Node::FunctionType)
            current->setChildTwo(((Function*)m_childTwo)->deepCopy(current));
        else {
            Terminal* currentChildTwo = (Terminal*) m_childTwo;
            Terminal* newChildTwo = new Terminal(currentChildTwo->terminalType(), current);
            Q_UNUSED(newChildTwo);
        }
    }

    return current;
}

int Function::compute(const pixNeighbor_t neighborhood) {

    int childOneCalculation;
    if (m_childOne->nodeType() == Node::FunctionType)
        childOneCalculation = ((Function*)m_childOne)->compute(neighborhood);
    else
        childOneCalculation = ((Terminal*)m_childOne)->terminalInt(neighborhood);

    Function::OperationFunction opFunc = functionPointer();

    int childTwoCalculation;
    if (m_childTwo->nodeType() == Node::FunctionType)
        childTwoCalculation = ((Function*)m_childTwo)->compute(neighborhood);
    else
        childTwoCalculation = ((Terminal*)m_childTwo)->terminalInt(neighborhood);

    int calculatedValue = (*opFunc)(childOneCalculation, childTwoCalculation);

    return calculatedValue;
}

QString Function::expression() {
    QString expressionString = "";
    QString childOneExpression = "";
    QString childTwoExpression = "";

    if (m_childOne->nodeType() == Node::FunctionType) {   //this child is a function, recurse and get this function's exp str
        Function* childOne = static_cast<Function*>(m_childOne);
        childOneExpression = childOne->expression();
        childOneExpression.prepend('(');
        childOneExpression.append(')');
    } else {
        Terminal* childOne = static_cast<Terminal*>(m_childOne);
        childOneExpression = childOne->terminalString();
    }

    //now get the expression for child two
    if (m_childTwo->nodeType() == Node::FunctionType) {
        Function* childTwo = static_cast<Function*>(m_childTwo);
        childTwoExpression = childTwo->expression();
        childTwoExpression.prepend('(');
        childTwoExpression.append(')');
    } else {
        Terminal* childTwo = static_cast<Terminal*>(m_childTwo);
        childTwoExpression = childTwo->terminalString();
    }

    //got the expressions for both children. Now, we need to insert our function operator between the two children

    QString operationExpression = functionString();

    expressionString = childOneExpression + operationExpression + childTwoExpression;

    return expressionString;
}

QString Function::serialize() {

    //Get all children in pre-order rank
    QList<Node*> inOrder = preOrder();

    // give each node 3 parts: type:id:parent, each segment separated by a colon, and each group by a space
    QString serialized = "";
    for (int i = 0; i < inOrder.size(); i++) {
        Node* node = inOrder.at(i);
        QString current = "";
        if (node->nodeType() == Node::FunctionType) {
            Function* f = static_cast<Function*>(node);
            current.append(f->functionString() + ":");
        } else if (node->nodeType() == Node::TerminalType) {
            Terminal* t = static_cast<Terminal*>(node);
            current.append(t->terminalString() + ":");
        }
        current.append(QString::number(node->id()) + ":");
        if (node->parentNode() == 0)
            current.append("-1");       //root parentid = -1
        else
            current.append(QString::number(node->parentNode()->id()));

        serialized.append(current + " ");
    }

    serialized.chop(1); //remove the tailing space
    return serialized;
}

void Function::deserialize(QString stream) {
    //We read this as a pre-order list

    //Assume the style is given as " type:id:parent", with a leading space to separate nodes and colons to separate data segments
    QStringList order = stream.split(" ", QString::SkipEmptyParts);
    QList<Node*> nodes;
    for (int i = 0; i < order.size(); i++) {
        QStringList segments = order.at(i).split(":", QString::SkipEmptyParts);
        QString nodeType = segments.at(0);
        int id = segments.at(1).toInt();
        int parentId = segments.at(2).toInt();

        Node* node = Util::stringToNode(nodeType);

        if (id == 0) {   //we're using this function as the root
            this->setType(static_cast<Function*>(node)->functionType());
            node = this;
        }

        if (node == 0)
            continue;

        node->setId(id);

        //Find its parent
        if (parentId != -1) {    //if the parent ID is -1, it means it's the root
            for (int j = 0; j < nodes.size(); j++) {
                int candidateId = nodes.at(j)->id();
                if (candidateId != parentId)
                    continue;

                //so we found it's parent
                Function* parent = static_cast<Function*>(nodes.at(j));
                if (parent->childOne() == 0)
                    parent->setChildOne(node);
                else
                    parent->setChildTwo(node);

                node->setParentNode(parent);
            }
        }
        nodes.append(node);
    }


    //The tree is all set up
}

QList<Node*> Function::preOrder(QList<Node*> children, int startingId) {

    this->setId(startingId++);
    children.append(this);

    if (m_childOne->nodeType() == Node::FunctionType) {
        Function *childOne = static_cast<Function*>(m_childOne);
        children.append(childOne->preOrder(QList<Node*>(), startingId));
    } else {
        Terminal* childOne = static_cast<Terminal*>(m_childOne);
        childOne->setId(startingId++);
        children.append(childOne);
    }

    //update the starting id to reflect changes from the left branching side
    startingId = children.last()->id() + 1;


    if (m_childTwo->nodeType() == Node::FunctionType) {
        Function *childTwo = static_cast<Function*>(m_childTwo);
        children.append(childTwo->preOrder(QList<Node*>(), startingId));
    } else {
        Terminal* childTwo = static_cast<Terminal*>(m_childTwo);
        childTwo->setId(startingId++);
        children.append(childTwo);
    }


    return children;
}
