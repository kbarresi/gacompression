#ifndef NODE_H
#define NODE_H

#include <QString>

class Function;
class Terminal;
class Node
{
public:
    enum NodeType { FunctionType, TerminalType };

    Node(Function* parent = 0);

    void setParentNode(Function* parentNode) { m_parent = parentNode; }
    Function* parentNode() { return m_parent; }

    NodeType nodeType() { return m_nodeType; }

    static int add(const int a, const int b) { return a + b; }
    static int sub(const int a, const int b) { return a - b; }
    static int mul(const int a, const int b) { return a * b; }
    static int div(const int a, const int b) { return a / qMax(b, 1); } //prevents divide by 1 errors
    static int min(const int a, const int b) { return qMin(a, b); }
    static int max(const int a, const int b) { return qMax(a, b); }


    void setId(int id) { m_id = id; }
    int id() { return m_id; }
protected:
    NodeType m_nodeType;
    Function* m_parent;



private:

    int m_id;
};

#endif // NODE_H
