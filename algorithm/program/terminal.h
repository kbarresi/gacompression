#ifndef TERMINAL_H
#define TERMINAL_H

#include "node.h"
#include "../../util/util.h"

#include <QString>
class Terminal : public Node
{
public:
    enum TerminalType { One, Two, Five, Ten, Hundred, Left, TopLeft, Top, TopRight};

    Terminal(TerminalType type, Function* parent = 0);
    ~Terminal();

    TerminalType terminalType() { return m_type; }
    void setType(TerminalType type) { m_type = type; }

    QString terminalString(const pixNeighbor_t neighborhood);
    QString terminalString();
    int terminalInt(const pixNeighbor_t neighborhood);

private:
    TerminalType m_type;
};

#endif // TERMINAL_H
