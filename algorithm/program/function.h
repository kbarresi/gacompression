#ifndef FUNCTION_H
#define FUNCTION_H

#include "node.h"

#include "../../util/util.h"

#include <QString>
class Function : public Node
{
public:
    enum FunctionType { Add, Subtract, Multiply, Divide, Min, Max };

    typedef int(*OperationFunction)(const int, const int);

    Function(FunctionType type, Function* parent = 0);
    ~Function();

    QList<Node*> getChildren();

    void setChildOne(Node* child);
    void setChildTwo(Node* child);
    void setChildren(Node* childOne, Node* childTwo) { setChildOne(childOne); setChildTwo(childTwo); }

    Node* childOne() { return m_childOne; }
    Node* childTwo() { return m_childTwo; }

    void setType(FunctionType type) { m_type = type; }
    FunctionType functionType() { return m_type; }

    QString expression(const pixNeighbor_t neighborhood);
    QString expression();

    QString functionString();
    OperationFunction functionPointer();

    Function* deepCopy(Function* parent);
    int compute(const pixNeighbor_t neighborhood);

    QString serialize();
    void deserialize(QString stream);

    QList<Node*> preOrder(QList<Node*> children = QList<Node*>(), int startingId = 0);
private:
    FunctionType m_type;

    Node* m_childOne;
    Node* m_childTwo;
};

#endif // FUNCTION_H
