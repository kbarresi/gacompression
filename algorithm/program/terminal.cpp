#include "terminal.h"

Terminal::Terminal(TerminalType type, Function* parent)
    : Node(parent)
{
    m_type = type;
    m_nodeType = Node::TerminalType;
}

Terminal::~Terminal() {
    //no children to delete....
}

QString Terminal::terminalString(const pixNeighbor_t neighborhood) {
    switch (m_type) {
    case One:
        return "1";
    case Two:
        return "2";
    case Five:
        return "5";
    case Ten:
        return "10";
    case Hundred:
        return "100";
    case Left:
        return QString::number(neighborhood.left);
    case TopLeft:
        return QString::number(neighborhood.topLeft);
    case Top:
        return QString::number(neighborhood.top);
    case TopRight:
        return QString::number(neighborhood.topRight);
    default:
        return "(ERROR)";
    }
}
QString Terminal::terminalString() {
    switch (m_type) {
    case One:
        return "1";
    case Two:
        return "2";
    case Five:
        return "5";
    case Ten:
        return "10";
    case Hundred:
        return "100";
    case Left:
        return "Left";
    case TopLeft:
        return "TopLeft";
    case Top:
        return "Top";
    case TopRight:
        return "TopRight";
    default:
        return "(ERROR)";
    }
}

int Terminal::terminalInt(const pixNeighbor_t neighborhood) {
    switch (m_type) {
    case One:
        return 1;
    case Two:
        return 2;
    case Five:
        return 5;
    case Ten:
        return 10;
    case Hundred:
        return 100;
    case Left:
        return neighborhood.left;
    case TopLeft:
        return neighborhood.topLeft;
    case Top:
        return neighborhood.top;
    case TopRight:
        return neighborhood.topRight;
    default:
        return 0;
    }
}
