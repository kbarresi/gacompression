#ifndef ERRORIMAGEFACTORY_H
#define ERRORIMAGEFACTORY_H

#include <QImage>
#include <QImage>

#include "population.h"
#include "../util/util.h"

class ErrorImageFactory
{
public:
    ErrorImageFactory(QImage &image, Function* red, Function* green, Function *blue);
    ~ErrorImageFactory();

    QVector<QVector<int> > redImage();
    QVector<QVector<int> > greenImage();
    QVector<QVector<int> > blueImage();

private:
    QImage m_inputImage;

    Function* m_redPrediction;
    Function* m_greenPrediction;
    Function* m_bluePrediction;

    QVector<QVector<int> > getImage(PixelGrabber pixelGrabber);

};

#endif // ERRORIMAGEFACTORY_H
