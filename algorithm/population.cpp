#include "population.h"

#include "chromosome.h"
#include "errorimagefactory.h"

#include "../algorithm/program/function.h"
#include "../algorithm/program/terminal.h"

#include <QDebug>
#include <QFutureWatcher>
#include <qtconcurrentmap.h>

Population::Population(QImage &image, PixelGrabber grabber, QObject *parent) :
    QObject(parent)
{
    this->grabber = grabber;
    m_generation = 0;
    m_image = 0;
    m_threadCount = QThread::idealThreadCount();

    m_image = (imagePtr) malloc(sizeof(int*) * image.width());
    for (int i = 0; i < image.width(); i++)
        m_image[i] = (int*) malloc(sizeof(int*) * image.height());

    m_workImages = (imagePtr*) malloc(sizeof(int*) * m_threadCount);
    for (int i = 0; i < m_threadCount; i++) {
        m_workImages[i] = (imagePtr) malloc(sizeof(int*) * image.width());
        for (int j = 0; j < image.width(); j++) {
            m_workImages[i][j] = (int*) malloc(sizeof(int*) * image.height());
        }
    }

    m_frequencyArray = (int**) malloc(sizeof(int*) * m_threadCount);
    m_probabilityArray = (double**) malloc(sizeof(double*) * m_threadCount);
    for (int i = 0; i < m_threadCount; i++) {
        m_frequencyArray[i] = (int*) malloc(sizeof(int*) * USHRT_MAX);
        m_probabilityArray[i] = (double*) malloc(sizeof(double*) * USHRT_MAX);
    }


    QRgb* colorPtr = 0;
    for (int y = 0; y < image.height(); y++) {
        colorPtr = (QRgb*) image.scanLine(y);
        for (int x = 0; x < image.width(); x++) {
            int color = grabber(*((QRgb*) colorPtr));
            m_image[x][y] = color;
            colorPtr++;
        }
    }

    m_worstCaseLevels = image.width() * image.height();
    m_width = image.width();
    m_height = image.height();

    qsrand((uint)QTime::currentTime().msec());

    qDebug() << "\tInput image entropy: " << processInput();
}
Population::~Population() {
    for (int i = 0; i < m_individuals.size(); i++) {
        Individual* individual = m_individuals.at(i);
        delete individual;
    }
    m_individuals.clear();

    for (int i = 0; i < m_threadCount; i++) {
        for (int j = 0; j < m_width; j++) {
            free(m_workImages[i][j]);
        }
        free(m_image[i]);
        free(m_frequencyArray[i]);
        free(m_probabilityArray[i]);
    }

}
bool individualFitnessComparator(Individual* i1, Individual *i2) {
    return i1->fitness() < i2->fitness();
}

void Population::run() {
    initialLayout();

    m_timer.start();
    while (!isGoodEnough() && m_generation < GENERATION_COUNT) {

        evaluate();
        select();
        reproduce();
        mutate();

        m_generation++;

        qDebug() << m_generation << errorEntropy(m_individuals.last());
    }

    evaluate(); //calculate the fitness for everything one last time
    qSort(m_individuals.begin(), m_individuals.end(), individualFitnessComparator); //sort it so the best one is last

    Function* bestFunction = m_individuals.last()->chromosome()->root()->deepCopy(0);   //make a copy of it so we can safely delete all individuals/bad functions
    qDebug() << "BEST FUNCTION: " << bestFunction->expression() << "\tEntropy: " << errorEntropy(m_individuals.last());

    qreal elapsed = (qreal)m_timer.elapsed() / (qreal)1000;
    qDebug() << "Time To Discovery: " << elapsed << " seconds";

    emit finished(bestFunction);
}

void fitnessCalculateWrapper(Individual* &i) { i->calculateFitness(); }

/**
 * Basic GA step functions
 */
void Population::evaluate() {
    if (ENABLE_PARALLEL) {
        for (int i = 0; i < m_individuals.size(); i += 4) {
            QList<Individual*> list;
            Individual *i1 = m_individuals.at(i);
            Individual *i2 = m_individuals.at(i + 1);
            Individual *i3 = m_individuals.at(i + 2);
            Individual *i4 = m_individuals.at(i + 3);

            i1->setThreadId(0);
            i2->setThreadId(1);
            i3->setThreadId(2);
            i4->setThreadId(3);

            list.append(i1);
            list.append(i2);
            list.append(i3);
            list.append(i4);

            QFutureWatcher<void> *watcher = new QFutureWatcher<void>();
            watcher->setFuture(QtConcurrent::map(list, fitnessCalculateWrapper));
            watcher->waitForFinished();
            delete watcher;
        }
    } else {
        for (int i = 0; i < m_individuals.size(); i++) {
            Individual* individual = m_individuals.at(i);
            individual->setChild(false);
            calculateFitness(individual);
        }
    }

}
void Population::select() {
    //The fitness scores have been calculated and the population infividuals are now sorted by their
    //fitness scores: Rank 0 = worst, Rank N = best

    qSort(m_individuals.begin(), m_individuals.end(), individualFitnessComparator);

    /*Now using the fitness rank, we have a linear ranking selection scheme.
    *
    *We reset the combined fitness F_i as:
    *
    * F_i = 2 - SP + 2(SP - 1)((i - 1)/(N - 1))
    * 	Where N = population size
    * 			SP = selective pressure [1.0, 2.0]
    * 			i is an individual's position, in terms of fitness
    *
    *
    *  This has the effect of smoothing out disproportionate fitnesses due
    *  to premature convergence.
    *
    *  Also note, the most fit  individual will remain the most fit, and is guaranteed a 100% chance of reproduction [elitism]
    */

    double N = m_individuals.size();
    double SP = 2;
    for (int i = 0; i < N; i++) {
        qreal position = i + 1;

        qreal linearFitnessValue = (qreal)2 - SP + ((qreal)2 * (SP - (qreal)1) * ((position - (qreal)1) / (N - (double)1)));
        m_individuals.at(i)->setFitness(linearFitnessValue);
    }

    //Now everything is scaled smoothly by fitness. Begin choosing which individuals will reproduce.
    int parentCount = 0;
    for (int i = 0; i < m_individuals.size(); i++) {
        Individual* individual = m_individuals.at(i);
        qreal matingProbability = individual->fitness() / SP;

        qreal rand = ((qreal)qrand() / (qreal)RAND_MAX);    //rand [0,1]
        if (matingProbability < rand) {
            individual->setForReproduction(true);
            parentCount++;
        }
    }

    if (parentCount % 2 != 0) {  //need even parents; if odd, kick out the least fit selected individual
        for (int i = 0; i < m_individuals.size(); i++) {
            Individual *ind = m_individuals.at(i);
            if (ind->isSetForReproduction()) {
                ind->setForReproduction(false);
                break;
            }
        }
    }

    //at this point, we have an even number of parents selected. Now we move to the next step and recombine

}
void Population::reproduce() {
    QList<Individual*> children;
    for (int i = m_individuals.size() - 1; i >= 0; i--) {
        Individual *parentOne = m_individuals.at(i);
        if (!parentOne->isSetForReproduction())
            continue;

        //otherwise, try to find a parent two;
        Individual* parentTwo = 0;
        for (int j = i; j <= 0; j--) {
            Individual* potentialParentTwo = m_individuals.at(j);
            if (!potentialParentTwo->isSetForReproduction())
                continue;
            parentTwo = potentialParentTwo;
            break;
        }

        if (parentTwo == 0) //wasn't able to get a second parent, so don't continue
            break;

        //otherwise, get the child from the two parents;
        Individual* child = mate(parentOne, parentTwo);
        children.append(child);
    }

    //We have a list of all children. remove enough of the worst individuals to make room for these
    for (int i = 0; i < children.size(); i++)
        m_individuals.pop_front();

    for (int i = 0; i < children.size(); i++)
        m_individuals.prepend(children.at(i)); //place the children at the front of the individuals list

}
void Population::mutate() {
    for (int i = 0; i < m_individuals.size() - 1; i++) {    //don't mutate the best one
        Individual* individual = m_individuals.at(i);
        if (individual->isChild())
            continue;
        mutateIndividual(individual);
    }
}



/**
 * Utility/backend functions
 */
void Population::initialLayout() {
    for (int i = 0; i < POPULATION_SIZE; i++) {
        Individual *individual = new Individual(m_width, m_height, m_worstEntropy);
        individual->setFrequencyArray(m_frequencyArray);
        individual->setImage(m_image);
        individual->setProbabilityArray(m_probabilityArray);
        individual->setWorkingArray(m_workImages);

        individual->setChild(false);

        Chromosome *chromosome = new Chromosome();

        //6 base types of JPEG prediction models, so get a random in [0,6] and cast to BASE_MODEL
        int baseModelId = qrand() % 7;
        JpegModel model = (JpegModel) baseModelId;
        Function* predictionFunction = Util::jpegModelToSexpr(model);

        chromosome->setRoot(predictionFunction);
        individual->setChromosome(chromosome);

        m_individuals.append(individual);
    }

}

bool Population::isGoodEnough() {
    return false;
}
void Population::calculateFitness(Individual *&individual) {
    /*
     *  When we calculate fitness, we want to minimize the output size of the image being compressed. That is, we want the
     *  error image to have the lowest entropy. Rather than calculating the error image AND running the entropy coder, we can
     *  take a bit of a shortcut and just use the number of discrete values in the error image, since this will directly
     *  correlate to the size of the codebook for encoding.
     *
     * tl;dr We're going to calculate the error image using the individual's prediction function, and count up the number of
     *       discrete values in the error image. The closer to 0, the better.
     * */

    Function* function = individual->chromosome()->root();

    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            m_workImages[0][x][y] = m_image[x][y];
        }
    }

    for (int i = 0; i < USHRT_MAX; i++) {
        m_frequencyArray[0][i] = 0;
        m_probabilityArray[0][i] = 0;
    }

    int inputLength = 0;

    for (int x = 1; x < m_width; x++) {
        for (int y = 1; y < m_height; y++) {

            pixNeighbor_t neighbors;
            neighbors.left = m_workImages[0][x-1][y];
            neighbors.topLeft = m_workImages[0][x-1][y-1];
            neighbors.top = m_workImages[0][x][y-1];
            neighbors.topRight = m_workImages[0][x][y];  //we don't really use topright


            const short predictedPixel = (short) function->compute(neighbors);
            const short errorPixel = m_workImages[0][x][y] - predictedPixel;
            const ushort index = errorPixel + SHRT_MAX;

            m_frequencyArray[0][index]++;  //we add SHRT_MAX to make sure the value isn't negative;
            m_workImages[0][x][y] = errorPixel;
            inputLength++;
        }
    }

    //now, create a list probability list of errorPixel value to likelyhood of being selected
    for (int i = 0; i < USHRT_MAX; i++) {
        int frequency = m_frequencyArray[0][i];
        if (frequency == 0)
            continue;
        qreal probability = (qreal)frequency / (qreal)inputLength;
        m_probabilityArray[0][i] = probability;
    }

    //Actual case: H = - sum(p_i * log_2(p_i) )
    qreal actualEntropy = 0;
    for (int i = 0; i < USHRT_MAX; i++) {
        qreal probability = m_probabilityArray[0][i];
        if (probability == 0)
            continue;
        actualEntropy += probability * Individual::log2(probability);
    }
    actualEntropy *= (qreal)(-1);


    qreal percentage = (qreal)1 - (actualEntropy / m_worstEntropy); // entropy/worst_entropy; lower is better
    individual->setFitness(percentage);
}

qreal Population::processInput() {
    for (int i = 0; i < USHRT_MAX; i++) {
        m_frequencyArray[0][i] = 0;
        m_probabilityArray[0][i] = 0;
    }

    int inputLength = 0;
    for (int x = 1; x < m_width; x++) {
        for (int y = 1; y < m_height; y++) {

            const short pixel = (short) m_image[x][y];

            const ushort index = pixel + SHRT_MAX;
            m_frequencyArray[0][index]++;
            inputLength++;
        }
    }

    //now, create a list probability list of errorPixel value to likelyhood of being selected
    for (int i = 0; i < USHRT_MAX; i++) {
        int frequency = m_frequencyArray[0][i];
        if (frequency == 0)
            continue;
        qreal probability = (qreal)frequency / (qreal)inputLength;
        m_probabilityArray[0][i] = probability;
    }


    //Actual case: H = - sum(p_i * log_2(p_i) )
    qreal actualEntropy = 0;
    for (int i = 0; i < USHRT_MAX; i++) {
        qreal probability = m_probabilityArray[0][i];
        if (probability == 0)
            continue;
        actualEntropy += probability * Individual::log2(probability);
    }
    actualEntropy *= (qreal)(-1);

    qreal worstEntropy = Individual::log2((qreal)inputLength);
    qDebug() << "Worst case entropy: " << worstEntropy;
    m_worstEntropy = worstEntropy;

    return actualEntropy;
}

qreal Population::errorEntropy(Individual *individual) {
    Function* function = individual->chromosome()->root();

    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            m_workImages[0][x][y] = m_image[x][y];
        }
    }
    for (int i = 0; i < USHRT_MAX; i++) {
        m_frequencyArray[0][i] = 0;
        m_probabilityArray[0][i] = 0;
    }

    int inputLength = 0;

    for (int x = 1; x < m_width; x++) {
        for (int y = 1; y < m_height; y++) {

            pixNeighbor_t neighbors;
            neighbors.left = m_workImages[0][x-1][y];
            neighbors.topLeft = m_workImages[0][x-1][y-1];
            neighbors.top = m_workImages[0][x][y-1];
            neighbors.topRight = m_workImages[0][x][y];  //we don't really use topright


            const short predictedPixel = function->compute(neighbors);
            const short errorPixel = m_workImages[0][x][y] - predictedPixel;

            const ushort index = errorPixel + SHRT_MAX;
            m_frequencyArray[0][index]++;

            m_workImages[0][x][y] = errorPixel;
            inputLength++;
        }
    }

    //now, create a list probability list of errorPixel value to likelyhood of being selected
    for (int i = 0; i < USHRT_MAX; i++) {
        int frequency = m_frequencyArray[0][i];
        if (frequency == 0)
            continue;
        qreal probability = (qreal)frequency / (qreal)inputLength;
        m_probabilityArray[0][i] = probability;
    }

    //Actual case: H = - sum(p_i * log_2(p_i) )
    qreal actualEntropy = 0;
    for (int i = 0; i < USHRT_MAX; i++) {
        qreal probability = m_probabilityArray[0][i];
        if (probability == 0)
            continue;
        actualEntropy += probability * Individual::log2(probability);
    }
    actualEntropy *= (qreal)(-1);
    return actualEntropy;
}

Individual* Population::mate(Individual* parentOne, Individual* parentTwo) {
    Individual *child = new Individual(m_width, m_height, m_worstEntropy);
    child->setFrequencyArray(m_frequencyArray);
    child->setImage(m_image);
    child->setProbabilityArray(m_probabilityArray);
    child->setWorkingArray(m_workImages);

    child->setChild(true);
    Chromosome *childChromosome = new Chromosome();
    childChromosome->setRoot(((Function*)parentOne)->deepCopy(0));

    //So the way this works is we pick a random node in parent one, and a random node in parent two. Then
    //what we do is simply switch them, which has the benefit of carrying along all sub-sexpr nodes with it

    //Rules: don't switch the root node. That's the only rule.

    //We can do this by first finding how many subnodes there are total, assigning each an incrementing ID. Then just
    //pick a random number that fits in the ID range

    //give parent one node IDs
    QMap<int, Node*> parentOneNodeIdMap;
    QList<Node*> parentOneChildrenList = ((Function*) parentOne)->getChildren();
    for (int i = 0; i < parentOneChildrenList.size(); i++)
        parentOneNodeIdMap.insert(i, parentOneChildrenList.at(i));

    //and do the same for parent 2s
    QMap<int, Node*> parentTwoNodeIdMap;
    QList<Node*> parentTwoChildrenList = ((Function*) parentTwo)->getChildren();
    for (int i = 0; i < parentTwoChildrenList.size(); i++)
        parentTwoNodeIdMap.insert(i, parentTwoChildrenList.at(i));

    //pick 2 random swap points
    int parentOneSwitchId = qrand() % parentOneChildrenList.size();
    int parentTwoSwitchId = qrand() % parentTwoChildrenList.size();

    //and get those nodes from the IDs
    Node* parentOneSwitchNode = parentOneNodeIdMap.value(parentOneSwitchId);
    Node* parentTwoSwitchNode = parentTwoNodeIdMap.value(parentTwoSwitchId);

    //grab those node's parents, since we need to update their child pointers
    Function* parentOneParentNode = parentOneSwitchNode->parentNode();  //we have to update these to point to the correct children
    Function* parentTwoParentNode = parentTwoSwitchNode->parentNode();

    //switch the parent node's child (the one we're swapping)
    if (parentOneParentNode->childOne() == parentOneSwitchNode) {
        if (parentTwoParentNode->childOne() == parentTwoSwitchNode) {
            parentOneParentNode->setChildOne(parentTwoSwitchNode);
            parentTwoParentNode->setChildOne(parentOneSwitchNode);
        } else {
            parentOneParentNode->setChildOne(parentTwoSwitchNode);
            parentTwoParentNode->setChildTwo(parentOneSwitchNode);
        }
    } else if (parentOneParentNode->childTwo() == parentOneSwitchNode) {
        if (parentTwoParentNode->childOne() == parentTwoSwitchNode) {
            parentOneParentNode->setChildTwo(parentTwoSwitchNode);
            parentTwoParentNode->setChildOne(parentOneSwitchNode);
        } else {
            parentOneParentNode->setChildTwo(parentTwoSwitchNode);
            parentTwoParentNode->setChildTwo(parentOneSwitchNode);
        }
    }

    child->setChromosome(childChromosome);
    return child;
}

void Population::mutateIndividual(Individual *individual) {
    QList<Node*> childrenList = individual->chromosome()->root()->getChildren();

    for (int i = 0; i < childrenList.size(); i++) {
        qreal rand = ((qreal)qrand() / (qreal)RAND_MAX);
        if (rand > MUTATION_PROB)
            continue;

        //so we want to mutate this node. Aka just find a different function/terminal
        Node* node = childrenList.at(i);
        if (node->nodeType() == Node::FunctionType) {
            Function* function = static_cast<Function*>(node);
            Function::FunctionType mutateTo = static_cast<Function::FunctionType>(qrand() % Function::Max); //cast random function type
            function->setType(mutateTo);
        } else {    //we have a terminal
            Terminal* terminal = static_cast<Terminal*>(node);
            Terminal::TerminalType mutateTo = static_cast<Terminal::TerminalType>(qrand() % Terminal::TopRight);
            terminal->setType(mutateTo);
        }
    }
}
