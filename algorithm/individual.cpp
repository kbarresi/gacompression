#include "individual.h"

#include "chromosome.h"
#include "program/function.h"
#include "program/terminal.h"

#include <QDebug>

Individual::Individual(int imgWidth, int imgHeight, qreal worstEntropy, QObject *parent) :
    QObject(parent)
{
    m_chromosome = 0;
    m_fitness = 0;
    m_isSetForReproduction = false;
    m_isChild = false;
    m_width = imgWidth;
    m_height = imgHeight;
    m_image = 0;
    m_worstEntropy = worstEntropy;
}

Individual::~Individual() {
    if (m_chromosome)
        delete m_chromosome;
}

void Individual::setChromosome(Chromosome *chromosome) {
    if (m_chromosome) {
        delete m_chromosome;
        m_chromosome = 0;
    }

    m_chromosome = chromosome;
}

void Individual::calculateFitness() {
    /*
     *  When we calculate fitness, we want to minimize the output size of the image being compressed. That is, we want the
     *  error image to have the lowest entropy. Rather than calculating the error image AND running the entropy coder, we can
     *  take a bit of a shortcut and just use the number of discrete values in the error image, since this will directly
     *  correlate to the size of the codebook for encoding.
     *
     * tl;dr We're going to calculate the error image using the individual's prediction function, and count up the number of
     *       discrete values in the error image. The closer to 0, the better.
     * */

    Function* function = chromosome()->root();

    for (int x = 0; x < m_width; x++) {
        for (int y = 0; y < m_height; y++) {
            m_workImage[m_threadId][x][y] = m_image[x][y];
        }
    }

    for (int i = 0; i < USHRT_MAX; i++) {
        m_frequencyArray[m_threadId][i] = 0;
        m_probabilityArray[m_threadId][i] = 0;
    }

    int inputLength = 0;

    for (int x = 1; x < m_width; x++) {
        for (int y = 1; y < m_height; y++) {

            pixNeighbor_t neighbors;
            neighbors.left = m_workImage[m_threadId][x-1][y];
            neighbors.topLeft = m_workImage[m_threadId][x-1][y-1];
            neighbors.top = m_workImage[m_threadId][x][y-1];
            neighbors.topRight = m_workImage[m_threadId][x][y];  //we don't really use topright


            const short predictedPixel = (short) function->compute(neighbors);
            const short errorPixel = m_workImage[m_threadId][x][y] - predictedPixel;
            const ushort index = errorPixel + SHRT_MAX;

            m_frequencyArray[m_threadId][index]++;  //we add SHRT_MAX to make sure the value isn't negative;
            m_workImage[m_threadId][x][y] = errorPixel;
            inputLength++;
        }
    }

    //now, create a list probability list of errorPixel value to likelyhood of being selected
    for (int i = 0; i < USHRT_MAX; i++) {
        int frequency = m_frequencyArray[m_threadId][i];
        if (frequency == 0)
            continue;
        qreal probability = (qreal)frequency / (qreal)inputLength;
        m_probabilityArray[m_threadId][i] = probability;
    }

    //Actual case: H = - sum(p_i * log_2(p_i) )
    qreal actualEntropy = 0;
    for (int i = 0; i < USHRT_MAX; i++) {
        qreal probability = m_probabilityArray[m_threadId][i];
        if (probability == 0)
            continue;
        actualEntropy += probability * log2(probability);
    }
    actualEntropy *= (qreal)(-1);


    qreal percentage = (qreal)1 - (actualEntropy / m_worstEntropy); // entropy/worst_entropy; lower is better
    setFitness(percentage);
}
