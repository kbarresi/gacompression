#include "chromosome.h"

#include "program/function.h"

Chromosome::Chromosome()
{
    m_root = 0;
}
Chromosome::~Chromosome() {
    if (m_root)
        delete m_root;
}
