#ifndef POPULATION_H
#define POPULATION_H

#include <QObject>
#include <QTime>

#define MUTATION_PROB 0.05
#define CROSSING_OVER_PROB 0.01
#define POPULATION_SIZE 200
#define GENERATION_COUNT 20
#define ENABLE_PARALLEL true


#include "../util/util.h"
#include "individual.h"

class Individual;
class Population : public QObject
{
    Q_OBJECT
public:
    explicit Population(QImage &image, PixelGrabber grabber, QObject *parent = 0);
    ~Population();

    PixelGrabber grabber;
signals:
    void finished(Function* function);
    void updated();

public slots:
    void run();

private:
    QList<Individual*> m_individuals;
    QTime m_timer;

    void evaluate();
    void select();
    void reproduce();
    void mutate();

    void initialLayout();

    bool isGoodEnough();
    void calculateFitness(Individual* &individual);

    qreal errorEntropy(Individual* individual);
    qreal processInput();

    Individual* mate(Individual* parentOne, Individual* parentTwo);
    void mutateIndividual(Individual *individual);

    imagePtr m_image;
    imagePtr* m_workImages;

    int** m_frequencyArray;
    double** m_probabilityArray;

    int m_width;
    int m_height;

    int m_threadCount;
    int m_worstCaseLevels;
    qreal m_worstEntropy;
    int m_generation;
};

#endif // POPULATION_H
