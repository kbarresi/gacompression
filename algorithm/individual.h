#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <QObject>
#include <qmath.h>

#define LOG_2 0.69314718055

typedef int** imagePtr;


class Chromosome;
class Individual : public QObject
{
    Q_OBJECT
public:
    explicit Individual(int imgWidth, int imgHeight, qreal worstEntropy, QObject *parent = 0);
    ~Individual();

    void setImage(imagePtr ar) { m_image = ar; }
    void setWorkingArray(imagePtr* ar) { m_workImage = ar; }
    void setFrequencyArray(imagePtr ar) { m_frequencyArray = ar; }
    void setProbabilityArray(double** ar) { m_probabilityArray = ar; }

    void setThreadId(int id) { m_threadId = id; }
    int getThreadId() { return m_threadId; }

    void setChromosome(Chromosome *chromosome);
    Chromosome* chromosome() { return m_chromosome; }

    void calculateFitness();
    void setFitness(qreal fitness) {m_fitness = fitness; }
    qreal fitness() {return m_fitness; }

    void setForReproduction(bool set) { m_isSetForReproduction = set; }
    bool isSetForReproduction() { return m_isSetForReproduction; }

    void setChild(bool set) { m_isChild = set; }
    bool isChild() { return m_isChild; }

    static qreal log2(qreal a) { return log((double)a) / (double)LOG_2; }

signals:

public slots:

private:
    Chromosome* m_chromosome;

    qreal m_fitness;

    bool m_isSetForReproduction;
    bool m_isChild;

    int m_width;
    int m_height;
    int m_threadId;

    qreal m_worstEntropy;

    imagePtr m_image;
    imagePtr* m_workImage;
    imagePtr m_frequencyArray;
    double** m_probabilityArray;
};

#endif // INDIVIDUAL_H
