#-------------------------------------------------
#
# Project created by QtCreator 2014-01-07T11:12:49
#
#-------------------------------------------------

QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GACompression
TEMPLATE = app


SOURCES += main.cpp\
    gui/gacompression.cpp \
    algorithm/population.cpp \
    algorithm/individual.cpp \
    algorithm/chromosome.cpp \
    algorithm/errorimagefactory.cpp \
    algorithm/program/function.cpp \
    algorithm/program/terminal.cpp \
    util/util.cpp \
    algorithm/program/node.cpp


HEADERS  += gui/gacompression.h \
    algorithm/population.h \
    algorithm/individual.h \
    algorithm/chromosome.h \
    algorithm/errorimagefactory.h \
    algorithm/program/function.h \
    algorithm/program/terminal.h \
    util/util.h \
    algorithm/program/node.h

FORMS    += gacompression.ui

RESOURCES +=
