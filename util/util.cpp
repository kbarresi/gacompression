#include "util.h"
#include "../algorithm/program/function.h"
#include "../algorithm/program/terminal.h"
#include "../algorithm/program/node.h"
#include <QDebug>
#include <QFileDialog>
#include <QProcess>

Util::Util()
{
}


QImage Util::reconstructImage(CompressedImage *image) {
    QImage composite = image->redErrorImage.copy();
    for (int y = 0; y < composite.height(); y++) {
        QRgb* color = (QRgb*) composite.scanLine(y);

        if (y == 0) {
            for (int x = 0; x < composite.width(); x++) {   //copy border pixels
                int red = image->redErrorData.at(y).at(x);
                int green = image->greenErrorData.at(y).at(x);
                int blue = image->blueErrorData.at(y).at(x);
                (*color) = qRgb(red, blue, green);
                color++;
            }
        } else {
            int red = image->redErrorData.at(y).at(0);
            int green = image->greenErrorData.at(y).at(0);
            int blue = image->blueErrorData.at(y).at(0);
            (*color) = qRgb(red, green, blue);
        }
    }

    QRgb* compositeColorPtr = NULL;     //pointer to the current row scan
    QRgb* compositeTrailingPtr = NULL;  //pointer to the previous row scan, needed for top neighbor pixels

    for (int y = 1; y < image->redErrorImage.height(); y++) {   //all error images are the same dimension, just use red dimensions for ease
        compositeColorPtr = (QRgb*) composite.scanLine(y);
        compositeTrailingPtr = (QRgb*) composite.scanLine(y - 1);

        for (int x = 1; x < image->redErrorImage.width(); x++) {
            int redError = image->redErrorData.at(y).at(x);
            int greenError = image->greenErrorData.at(y).at(x);
            int blueError = image->blueErrorData.at(y).at(x);

            //use the prediction model

            pixNeighbor_t redNeighbors;
            redNeighbors.left = qRed(*((QRgb*) compositeColorPtr - 1));
            redNeighbors.topLeft = qRed(*((QRgb*) compositeColorPtr - 1));
            redNeighbors.top = qRed((*(QRgb*) compositeColorPtr));
            redNeighbors.topRight = qRed((*(QRgb*) compositeColorPtr + 1));

            pixNeighbor_t greenNeighbors;
            greenNeighbors.left = qGreen(*((QRgb*) compositeColorPtr - 1));
            greenNeighbors.topLeft = qGreen(*((QRgb*) compositeColorPtr - 1));
            greenNeighbors.top = qGreen((*(QRgb*) compositeColorPtr));
            greenNeighbors.topRight = qGreen((*(QRgb*) compositeColorPtr + 1));

            pixNeighbor_t blueNeighbors;
            blueNeighbors.left = qBlue(*((QRgb*) compositeColorPtr - 1));
            blueNeighbors.topLeft = qBlue(*((QRgb*) compositeColorPtr - 1));
            blueNeighbors.top = qBlue((*(QRgb*) compositeColorPtr));
            blueNeighbors.topRight = qBlue((*(QRgb*) compositeColorPtr + 1));

            int redModelColor = image->redPredictionFunction->compute(redNeighbors);
            int greenModelColor = image->greenPredictionFunction->compute(greenNeighbors);
            int blueModelColor = image->bluePredictionFunction->compute(blueNeighbors);

            int compositeRedColor = redModelColor + redError;
            int compositeGreenColor = greenModelColor + greenError;
            int compositeBlueColor = blueModelColor + blueError;

            (*compositeColorPtr) = qRgb(compositeRedColor, compositeGreenColor, compositeBlueColor);

            compositeColorPtr++;
            compositeTrailingPtr++;
        }
    }

    return composite;
}



/**
  * Image/1-D vector conversion functions
 */
QVector<QVector<int> > Util::image2Vector(const QImage &image, PixelGrabber color) {

    QVector<QVector<int> > outputArray;
    for (int i = 0; i < image.height(); i++) {
        QVector<int> list;

        QRgb* colorPtr = (QRgb*) image.scanLine(i);
        for (int j = 0; j < image.width(); j++) {
            list.append((*color)(*colorPtr));   //returns red, green, or blue color, depending on how this was called
            colorPtr++;
        }
        outputArray.append(list);
    }

    return outputArray;
}
QImage Util::vector2Image(const QVector<QVector<int> > input) {
    QSize size(input.first().size(), input.size());
    QImage output(size, QImage::Format_ARGB32);

    for (int y = 0; y < output.height(); y++) {
        QRgb* colorPtr = (QRgb*) output.scanLine(y);
        for (int x = 0; x < output.width(); x++) {
            int pixel = qAbs(input.at(y).at(x));
            (*colorPtr) = qRgb(pixel, pixel, pixel);
            colorPtr++;
        }
    }

    return output;
}


Function* Util::jpegModelToSexpr(JpegModel model) {
    Function* function = 0;
    switch (model) {
    case J1: {
        //Prediction: P[m-1, n]
        // = Left
        function = new Function(Function::Multiply, 0);
        Terminal *leftChild = new Terminal(Terminal::Left, function);
        Terminal *rightChild = new Terminal(Terminal::One, function);

        Q_UNUSED(leftChild);
        Q_UNUSED(rightChild);
        break;
    }
    case J2: {
        //Prediction: P[m,n-1]
        // = Top
        function = new Function(Function::Multiply, 0);
        Terminal *leftChild = new Terminal(Terminal::Top, function);
        Terminal *rightChild = new Terminal(Terminal::One, function);

        Q_UNUSED(leftChild);
        Q_UNUSED(rightChild);
        break;
    }
    case J3: {
        //Prediction: P[m-1,n-1]
        // = TopLeft
        function = new Function(Function::Multiply, 0);
        Terminal *leftChild = new Terminal(Terminal::TopLeft, function);
        Terminal *rightChild = new Terminal(Terminal::One, function);

        Q_UNUSED(leftChild);
        Q_UNUSED(rightChild);
        break;
    }
    case J4: {
        //Prediction: P[m-1,n] + P[m,n-1] + P[m-1,n-1]
        // = Left + Top + TopLeft
        function = new Function(Function::Add, 0);

        Function* leftAdd = new Function(Function::Add, function);
        Terminal* leftAddC1 = new Terminal(Terminal::Left, leftAdd);
        Terminal* leftAddC2 = new Terminal(Terminal::Top, leftAdd);

        Function* rightAdd = new Function(Function::Multiply, function);
        Terminal* rightAddC1 = new Terminal(Terminal::TopLeft, rightAdd);
        Terminal* rightAddC2 = new Terminal(Terminal::One, rightAdd);

        Q_UNUSED(leftAddC1);
        Q_UNUSED(leftAddC2);
        Q_UNUSED(rightAddC1);
        Q_UNUSED(rightAddC2);
        break;
    }
    case J5: {
        //Prediction: P[m-1,n] + ( (P[m,n-1] - P[m-1,n-1]) / 2)
        // = Left + ( (Top - TopLeft) / 2)
        function = new Function(Function::Add, 0);

        Terminal* leftPixTerminal = new Terminal(Terminal::Left, function);
        Function* divideFunction = new Function(Function::Divide, function);

        Function* subtractFunction = new Function(Function::Subtract, divideFunction);
        Terminal* topPixTerminal = new Terminal(Terminal::Top, subtractFunction);
        Terminal* topLeftPixTerminal = new Terminal(Terminal::TopLeft, subtractFunction);

        Terminal* divideByTwo = new Terminal(Terminal::Two, divideFunction);


        Q_UNUSED(leftPixTerminal);
        Q_UNUSED(topPixTerminal);
        Q_UNUSED(topLeftPixTerminal);
        Q_UNUSED(divideByTwo);
        break;
    }
    case J6: {
        //Prediction: P[m,n-1] + ( (P[m-1,n] - P[m-1,n-1]) / 2)
        // = Top + ( (Left - TopLeft) / 2)
        function = new Function(Function::Add, 0);

        Terminal* topPixTerminal = new Terminal(Terminal::Top, function);
        Function* divideFunction = new Function(Function::Divide, function);

        Function* subtractFunction = new Function(Function::Subtract, divideFunction);
        Terminal* leftPixTerminal = new Terminal(Terminal::Left, subtractFunction);
        Terminal* topLeftPixTerminal = new Terminal(Terminal::TopLeft, subtractFunction);

        Terminal* divideByTwo = new Terminal(Terminal::Two, divideFunction);

        Q_UNUSED(topPixTerminal);
        Q_UNUSED(leftPixTerminal);
        Q_UNUSED(topLeftPixTerminal);
        Q_UNUSED(divideByTwo);
        break;
    }
    case J7: {
        //Prediction: (P[m-1,n] + P[m,n-1]) / 2
        // = (Left + Top) / 2
        function = new Function(Function::Divide, 0);

        Function* addFunction = new Function(Function::Add, function);
        Terminal* leftPixTerminal = new Terminal(Terminal::Left, addFunction);
        Terminal* topPixTerminal = new Terminal(Terminal::Top, addFunction);

        Terminal* divideByTwoTerminal = new Terminal(Terminal::Two, function);

        Q_UNUSED(leftPixTerminal);
        Q_UNUSED(topPixTerminal);
        Q_UNUSED(divideByTwoTerminal);
        break;
    }
    default: {
        function = new Function(Function::Multiply, 0);
        Terminal* t1 = new Terminal(Terminal::One, function);
        Terminal* t2 = new Terminal(Terminal::One, function);

        Q_UNUSED(t1);
        Q_UNUSED(t2);
        break;
    }
    }

    return function;
}

Node* Util::stringToNode(QString in) {
    if (in == "+")
        return new Function(Function::Add, 0);
    else if (in == "-")
        return new Function(Function::Subtract, 0);
    else if (in == "*")
        return new Function(Function::Multiply, 0);
    else if (in == "/")
        return new Function(Function::Divide, 0);
    else if (in == "min")
        return new Function(Function::Min, 0);
    else if (in == "max")
        return new Function(Function::Max, 0);
    else if (in == "1")
        return new Terminal(Terminal::One, 0);
    else if (in == "2")
        return new Terminal(Terminal::Two, 0);
    else if (in == "5")
        return new Terminal(Terminal::Five, 0);
    else if (in == "10")
        return new Terminal(Terminal::Ten, 0);
    else if (in == "100")
        return new Terminal(Terminal::Hundred, 0);
    else if (in == "Left")
        return new Terminal(Terminal::Left, 0);
    else if (in == "TopLeft")
        return new Terminal(Terminal::TopLeft, 0);
    else if (in == "Top")
        return new Terminal(Terminal::Top, 0);
    else if (in == "TopRight")
        return new Terminal(Terminal::TopRight);
    else
        return 0;
}

FileSaver::FileSaver () {
}

void FileSaver::save(CompressedImage *image) {

    /* FILE STRUCTURE
     *
     * Header (2 bytes);
     * Separator (4 bytes);
     * Image width (4 bytes);
     * Image height (4 bytes);
     * Red Prediction Model Length (8 bytes);
     * Red Prediction Model
     * Green Prediction Model Length (8 bytes);
     * Green Prediction Model
     * Blue Prediction Model Length (8 bytes)
     * Blue Prediction Model
     * Separator (4 bytes);
     * Error Image Red Length (8 bytes);
     * Error Image Red(??? bytes);
     * Error Image Green Length (8 bytes);
     * Error Image Green(??? bytes);
     * Error Image Blue Length (8 bytes);
     * Error Image Blue(??? bytes)
     * Separator (4 bytes);
     * Red Border Pixel Length (8 bytes);
     * Red Border Pixels (??? bytes, <= [width + height - 1]);
     * Green Border Pixel Length (8 bytes);
     * Green Border Pixels (??? bytes, <= [width + height - 1]);
     * Blue Border Pixel Length (8 bytes);
     * Blue Border Pixels (??? bytes, <= [width + height - 1]);
     * Separator (4 bytes);
     * Separator (4 bytes);
     */

    //Basically, we're going to put all the data we need into char arrays, and then write it to file.

    //Get the file to save it to
    QString filename = QFileDialog::getSaveFileName(0, "Save image", QDir::currentPath(), "Images (*.epp)");
    if (filename.isNull())
        return;

#ifdef Q_OS_LINUX
    filename.append(".epp");
#endif

    QString errorImageUncompressedFilename = filename + "_err1";
    QString errorImageCompressedFilename = filename + "_err2";


    //Setup the header and separator bytes
    char* header = (char*) malloc(sizeof(char*) * HEADER_LENGTH);
    header[0] = (char)0xAB;
    header[1] = (char)0xCD;

    char* separator = (char*) malloc(sizeof(char*) * SEPARATOR_LENGTH);
    separator[0] = (char)0xDE;
    separator[1] = (char)0xAD;
    separator[2] = (char)0xBE;
    separator[3] = (char)0xEF;

    //And grab the width/height of the error images (use the red error image, but they're all the same dimensions
    char* width = (char*) malloc(sizeof(char*) * WIDTH_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(image->redErrorImage.width(), width, WIDTH_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(width, WIDTH_LENGTH, "%d", image->errorImage.width());
#endif

    char* height = (char*) malloc(sizeof(char*) * HEIGHT_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(image->redErrorImage.height(), height, HEIGHT_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(height, HEIGHT_LENGTH, "%d", image->errorImage.height());
#endif


    //And the prediction functions too, for each color channel
    QByteArray redPredictionModelByteArray = image->redPredictionFunction->serialize().toLatin1();
    QByteArray greenPredictionModelByteArray = image->greenPredictionFunction->serialize().toLatin1();
    QByteArray bluePredictionModelByteArray = image->bluePredictionFunction->serialize().toLatin1();
    char* redPredictionModel = redPredictionModelByteArray.data();
    char* greenPredictionModel = greenPredictionModelByteArray.data();
    char* bluePredictionModel = bluePredictionModelByteArray.data();
    char* redPredictionModelLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    char* greenPredictionModelLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    char* bluePredictionModelLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(redPredictionModelByteArray.size(), redPredictionModelLength, ERROR_IMAGE_LENGTH, 10);
    _ltoa_s(greenPredictionModelByteArray.size(), greenPredictionModelLength, ERROR_IMAGE_LENGTH, 10);
    _ltoa_s(bluePredictionModelByteArray.size(), bluePredictionModelLength, ERROR_IMAGE_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(redPredictionModelLength, ERROR_IMAGE_LENGTH, "%d", redPredictionModelByteArray.size());
    snprintf(greenPredictionModelLength, ERROR_IMAGE_LENGTH, "%d", greenPredictionModelByteArray.size());
    snprintf(bluePredictionModelLength, ERROR_IMAGE_LENGTH, "%d", bluePredictionModelByteArray.size());
#endif


    //Now we'll deal with the error image streams. Each channel has to be arithmetically encoded, for which
    //we'll use an external executable to deal with. So for each channel, we need to write to a file, run the
    //encoder, and read the compressed bytes


    //We'll start with the red error image data

    //Open the file that we'e going to store the uncompressed data in
    QFile errorImageUncompressedFile(errorImageUncompressedFilename);
    if (!errorImageUncompressedFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        qDebug() << "Error opening red error image uncompressed file: " << errorImageUncompressedFilename;
        return;
    }


    QDataStream stream(&errorImageUncompressedFile);
    for (int i = 1; i < image->redErrorData.size(); i++) { //we ignore the first row and first column, since those are the border pixels
        QVector<int> row = image->redErrorData.at(i);
        QByteArray currentRow;
        for (int  j = 1; j < row.size(); j++)
            currentRow.append(row.at(j));

        stream.writeRawData(currentRow.constData(), currentRow.size());
    }
    errorImageUncompressedFile.close();


    //Set up the process call to the arithmetic encoder
#ifdef Q_OS_WIN
    QString encoderProgram = QString("../GACompression/resources/ac.exe");
#endif
#ifdef Q_OS_LINUX
    QString encoderProgram = QString("../GACompression/resources/ac");
#endif

    QStringList parameters;
    parameters << errorImageUncompressedFilename << errorImageCompressedFilename;   //ac.exe writes the compressed data to the second file

    QProcess *proc = new QProcess(0);
    proc->start(encoderProgram, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    //so now the file ending in *_tmp2 contains the encoded form of the error image.


    //Now just grab the encoded data from the file produced so we can manipulate it. We'll read it later
    QFile errorImageCompressedFile(errorImageCompressedFilename);
    if (!errorImageCompressedFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open the compressed error image file";
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }

    QByteArray redCompressedErrorByteArray = errorImageCompressedFile.readAll();
    char* redCompressedError = redCompressedErrorByteArray.data();
    char* redErrorLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(redCompressedErrorByteArray.size(), redErrorLength, ERROR_IMAGE_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(redErrorLength, ERROR_IMAGE_LENGTH, "%d", compressedRedErrorByteArray.size());
#endif
    errorImageCompressedFile.close();


    //Now do the same thing for the green channel
    if (!errorImageUncompressedFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        qDebug() << "Error opening green error image uncompressed file: " << errorImageUncompressedFilename;
        return;
    }
    stream.setDevice(&errorImageUncompressedFile);
    for (int i = 1; i < image->greenErrorData.size(); i++) { //we ignore the first row and first column, since those are the border pixels
        QVector<int> row = image->greenErrorData.at(i);
        QByteArray currentRow;
        for (int  j = 1; j < row.size(); j++)
            currentRow.append(row.at(j));

        stream.writeRawData(currentRow.constData(), currentRow.size());
    }
    errorImageUncompressedFile.close();

    proc = new QProcess(0);
    proc->start(encoderProgram, parameters);
    proc->waitForFinished();
    proc->deleteLater();

    //get the green channel compressed data...
    if (!errorImageCompressedFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open the compressed green error image file";
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }

    QByteArray greenCompressedErrorByteArray = errorImageCompressedFile.readAll();
    char* greenCompressedError = greenCompressedErrorByteArray.data();
    char* greenErrorLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(greenCompressedErrorByteArray.size(), greenErrorLength, ERROR_IMAGE_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(greenErrorLength, ERROR_IMAGE_LENGTH, "%d", compressedGreenErrorByteArray.size());
#endif
    errorImageCompressedFile.close();




    //And finally, for the Blue channel:
    if (!errorImageUncompressedFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        qDebug() << "Error opening blue error image uncompressed file: " << errorImageUncompressedFilename;
        return;
    }
    stream.setDevice(&errorImageUncompressedFile);
    for (int i = 1; i < image->blueErrorData.size(); i++) { //we ignore the first row and first column, since those are the border pixels
        QVector<int> row = image->blueErrorData.at(i);
        QByteArray currentRow;
        for (int  j = 1; j < row.size(); j++)
            currentRow.append(row.at(j));

        stream.writeRawData(currentRow.constData(), currentRow.size());
    }
    errorImageUncompressedFile.close();

    proc = new QProcess(0);
    proc->start(encoderProgram, parameters);
    proc->waitForFinished();
    proc->deleteLater();

    //get the blue channel compressed data...
    if (!errorImageCompressedFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open the compressed blue error image file";
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }

    QByteArray blueCompressedErrorByteArray = errorImageCompressedFile.readAll();
    char* blueCompressedError = blueCompressedErrorByteArray.data();
    char* blueErrorLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(blueCompressedErrorByteArray.size(), blueErrorLength, ERROR_IMAGE_LENGTH, 10);
#elif Q_OS_LINUX
    snprintf(blueErrorLength, ERROR_IMAGE_LENGTH, "%d", compressedBlueErrorByteArray.size());
#endif
    errorImageCompressedFile.close();

    //So we have the red, blue, and green compressed values saved in memory and ready to go.


    //Now we need to compress the border pixels. Again, do once for the red, green, and blue channels

    //Read the red border pixel data into a char array
    int redDecompressedBorderLenth = image->redErrorImage.width() + image->redErrorImage.height() - 1;
    char* redDecompressedBorder = (char*) malloc(sizeof(char*) * redDecompressedBorderLenth);
    int decompressedBorderIndex = 0;
    for (int y = image->redErrorImage.height() - 1; y >= 0; y--) {
        QRgb* colorPtr = (QRgb*) image->redErrorImage.scanLine(y);
        if (y == 0) {
            for (int x = 0; x < image->redErrorImage.width(); x++) {
                redDecompressedBorder[decompressedBorderIndex++] = (uchar)qRed(*colorPtr);
                colorPtr++;
            }
        } else {
            redDecompressedBorder[decompressedBorderIndex++] = (uchar)qRed(*colorPtr);
        }
    }


    //do the same for green...
    int greenDecompressedBorderLenth = image->greenErrorImage.width() + image->greenErrorImage.height() - 1;
    char* greenDecompressedBorder = (char*) malloc(sizeof(char*) * greenDecompressedBorderLenth);
    decompressedBorderIndex = 0;
    for (int y = image->greenErrorImage.height() - 1; y >= 0; y--) {
        QRgb* colorPtr = (QRgb*) image->greenErrorImage.scanLine(y);
        if (y == 0) {
            for (int x = 0; x < image->greenErrorImage.width(); x++) {
                greenDecompressedBorder[decompressedBorderIndex++] = qGreen(*colorPtr);
                colorPtr++;
            }
        } else {
            greenDecompressedBorder[decompressedBorderIndex++] = qGreen(*colorPtr);
        }
    }

    //and blue
    int blueDecompressedBorderLength = image->blueErrorImage.width() + image->blueErrorImage.height() - 1;
    char* blueDecompressedBorder = (char*) malloc(sizeof(char*) * blueDecompressedBorderLength);
    decompressedBorderIndex = 0;
    for (int y = image->blueErrorImage.height() - 1; y >= 0; y--) {
        QRgb* colorPtr = (QRgb*) image->blueErrorImage.scanLine(y);
        if (y == 0) {
            for (int x = 0; x < image->blueErrorImage.width(); x++) {
                blueDecompressedBorder[decompressedBorderIndex++] = qBlue(*colorPtr);
                colorPtr++;
            }
        } else
            blueDecompressedBorder[decompressedBorderIndex++] = qBlue(*colorPtr);
    }

    for (int i = 0; i < 50; i++) {
        qDebug("%d %d %d", redDecompressedBorder[i], greenDecompressedBorder[i], blueDecompressedBorder[i]);
    }

    //Read the first channel into a file so we can gzip it
    QString borderPixelUncompressedFilename = filename + "_border";
    QFile borderPixelUncompressedFile(borderPixelUncompressedFilename);
    if (!borderPixelUncompressedFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
        qDebug() << "Unable to save border pixels; escaping";   //cleanup and leave
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }
    stream.setDevice(&borderPixelUncompressedFile);
    stream.writeRawData(redDecompressedBorder, redDecompressedBorderLenth);
    borderPixelUncompressedFile.close();
    free(redDecompressedBorder);    //don't need the decompressed bytes anymore

    //now we use gzip on that file
#ifdef Q_OS_WIN
    QString gzip = QString("../GACompression/resources/gzip.exe");
#endif
#ifdef Q_OS_LINUX
    QString gzip = QString("../GACompression/resources/gzip");
#endif
    parameters.clear();
    parameters << "--best" << borderPixelUncompressedFilename;
    QProcess *gzipBorderPixelProc = new QProcess(0);
    gzipBorderPixelProc->start(gzip, parameters);
    gzipBorderPixelProc->waitForFinished();
    gzipBorderPixelProc->deleteLater();


    //And read the compressed data back in, the file has .gz appended
    QString borderPixelCompressedFilename = borderPixelUncompressedFilename+ ".gz";
    QFile borderPixelCompressedFile(borderPixelCompressedFilename);
    if (!borderPixelCompressedFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open compressed border pixels; escaping";
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        QFile::remove(borderPixelUncompressedFilename);
        return;
    }

    QByteArray redCompressedBorderByteArray = borderPixelCompressedFile.readAll();
    char* redCompressedBorder = redCompressedBorderByteArray.data();
    char* redCompressedBorderLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
#ifdef Q_OS_WIN
    _ltoa_s(redCompressedBorderByteArray.size(), redCompressedBorderLength, ERROR_IMAGE_LENGTH, 10);
#elif
    snprintf(redCompressedBorderLength, ERROR_IMAGE_LENGTH, "%d", redCompressedBorderByteArray.size());
#endif
    borderPixelCompressedFile.remove();
    borderPixelCompressedFile.close();


    //Now compress the green data
     if (!borderPixelUncompressedFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to save green border pixels; escaping";   //cleanup and leave
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }
     stream.setDevice(&borderPixelUncompressedFile);
     stream.writeRawData(greenDecompressedBorder, greenDecompressedBorderLenth);
     borderPixelUncompressedFile.close();
     free(greenDecompressedBorder);    //don't need the decompressed bytes anymore

     gzipBorderPixelProc = new QProcess(0);
     gzipBorderPixelProc->start(gzip, parameters);
     gzipBorderPixelProc->waitForFinished();
     gzipBorderPixelProc->deleteLater();

     if (!borderPixelCompressedFile.open(QIODevice::ReadOnly)) {
         qDebug() << "Unable to open compressed border pixels; escaping";
         QFile::remove(errorImageUncompressedFilename);
         QFile::remove(errorImageCompressedFilename);
         QFile::remove(filename);
         QFile::remove(borderPixelUncompressedFilename);
         return;
     }
     QByteArray greenCompressedBorderByteArray = borderPixelCompressedFile.readAll();
     char* greenCompressedBorder = greenCompressedBorderByteArray.data();
     char* greenCompressedBorderLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
 #ifdef Q_OS_WIN
     _ltoa_s(greenCompressedBorderByteArray.size(), greenCompressedBorderLength, ERROR_IMAGE_LENGTH, 10);
 #elif
     snprintf(greenCompressedBorderLength, ERROR_IMAGE_LENGTH, "%d", greenCompressedBorderByteArray.size());
 #endif
     borderPixelCompressedFile.remove();
     borderPixelCompressedFile.close();


     //And finally the blue channel
     if (!borderPixelUncompressedFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to save blue border pixels; escaping";   //cleanup and leave
        QFile::remove(errorImageUncompressedFilename);
        QFile::remove(errorImageCompressedFilename);
        QFile::remove(filename);
        return;
    }
     stream.setDevice(&borderPixelUncompressedFile);
     stream.writeRawData(blueDecompressedBorder, blueDecompressedBorderLength);
     borderPixelUncompressedFile.close();
     free(blueDecompressedBorder);    //don't need the decompressed bytes anymore

     gzipBorderPixelProc = new QProcess(0);
     gzipBorderPixelProc->start(gzip, parameters);
     gzipBorderPixelProc->waitForFinished();
     gzipBorderPixelProc->deleteLater();

     if (!borderPixelCompressedFile.open(QIODevice::ReadOnly)) {
         qDebug() << "Unable to open compressed border pixels; escaping";
         QFile::remove(errorImageUncompressedFilename);
         QFile::remove(errorImageCompressedFilename);
         QFile::remove(filename);
         QFile::remove(borderPixelUncompressedFilename);
         return;
     }
     QByteArray blueCompressedBorderByteArray = borderPixelCompressedFile.readAll();
     char* blueCompressedBorder = blueCompressedBorderByteArray.data();
     char* blueCompressedBorderLength = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
 #ifdef Q_OS_WIN
     _ltoa_s(blueCompressedBorderByteArray.size(), blueCompressedBorderLength, ERROR_IMAGE_LENGTH, 10);
 #elif
     snprintf(blueCompressedBorderLength, ERROR_IMAGE_LENGTH, "%d", blueCompressedBorderByteArray.size());
 #endif


    //close the old files and delete them
    borderPixelCompressedFile.remove();
    errorImageCompressedFile.remove();
    borderPixelCompressedFile.close();
    errorImageCompressedFile.close();

    QFile::remove(errorImageUncompressedFilename);
    QFile::remove(errorImageCompressedFilename);
    QFile::remove(borderPixelUncompressedFilename);
    QFile::remove(borderPixelCompressedFilename);


    QFile eppImageFile(filename);
    if (!eppImageFile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {  //open it one last time to write the entire file structure
        qDebug() << "Unable to open the EPP output file!";
        free(header);
        free(separator);
        free(width);
        free(height);
        return;
    }


    //Now write everything!
    stream.setDevice(&eppImageFile);
    stream.writeRawData(header, HEADER_LENGTH);
    stream.writeRawData(separator, SEPARATOR_LENGTH);
    stream.writeRawData(width, WIDTH_LENGTH);
    stream.writeRawData(height, HEIGHT_LENGTH);

    stream.writeRawData(redPredictionModelLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(redPredictionModel, redPredictionModelByteArray.size());
    stream.writeRawData(greenPredictionModelLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(greenPredictionModel, greenPredictionModelByteArray.size());
    stream.writeRawData(bluePredictionModelLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(bluePredictionModel, bluePredictionModelByteArray.size());
    stream.writeRawData(separator, SEPARATOR_LENGTH);

    stream.writeRawData(redErrorLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(redCompressedError, redCompressedErrorByteArray.size());
    stream.writeRawData(greenErrorLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(greenCompressedError, greenCompressedErrorByteArray.size());
    stream.writeRawData(blueErrorLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(blueCompressedError, blueCompressedErrorByteArray.size());
    stream.writeRawData(separator, SEPARATOR_LENGTH);

    stream.writeRawData(redCompressedBorderLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(redCompressedBorder, redCompressedBorderByteArray.size());
    stream.writeRawData(greenCompressedBorderLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(greenCompressedBorder, greenCompressedBorderByteArray.size());
    stream.writeRawData(blueCompressedBorderLength, ERROR_IMAGE_LENGTH);
    stream.writeRawData(blueCompressedBorder, blueCompressedBorderByteArray.size());

    stream.writeRawData(separator, SEPARATOR_LENGTH);
    stream.writeRawData(separator, SEPARATOR_LENGTH);

    //free up whatever we allocated
    free(header);
    free(separator);
    free(width);
    free(height);

    qDebug() << "\tred pixel border length: " << redDecompressedBorderLenth << ":" << redCompressedBorderByteArray.size();
    qDebug() << "\tgreen pixel border length: " << greenDecompressedBorderLenth << ":" << greenCompressedBorderByteArray.size();
    qDebug() << "\tblue pixel border length: " << blueDecompressedBorderLength << ":" << blueCompressedBorderByteArray.size();


    qDebug() << "Successfully saved compressed image: " << eppImageFile.size() << "bytes";
    eppImageFile.close();
}


CompressedImage::CompressedImage() {
}

CompressedImage::~CompressedImage() {
}
