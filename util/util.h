#ifndef UTIL_H
#define UTIL_H

#include <QImage>
#include <QHash>

#define HEADER_LENGTH 2
#define SEPARATOR_LENGTH 4
#define WIDTH_LENGTH 4
#define HEIGHT_LENGTH 4
#define ERROR_IMAGE_LENGTH 8

typedef uchar (*PredictionFunction)(const QImage*, const uchar, const QPoint);
typedef int (*PixelGrabber)(QRgb);

typedef struct {
    int left;
    int topLeft;
    int top;
    int topRight;
} pixNeighbor_t;

enum JpegModel { J1, J2, J3, J4, J5, J6, J7 };


class Function;
class CompressedImage;
class FileSaver;
class Node;
class Util
{
public:
    Util();


    static QImage reconstructImage(CompressedImage *image);

    static QVector<QVector<int> > image2Vector(const QImage &image, PixelGrabber color);
    static QImage vector2Image(const QVector<QVector<int> > input);

    static Function* jpegModelToSexpr(JpegModel model);
    static Node* stringToNode(QString in);

};



class FileSaver
{
public:
    FileSaver();
    ~FileSaver() {}

    void save(CompressedImage *image);
    QByteArray encoded() { return m_encoded; }

private:

    QByteArray m_encoded;
};

class CompressedImage
{
public:
    CompressedImage();
    ~CompressedImage();

    QImage redErrorImage;
    QImage greenErrorImage;
    QImage blueErrorImage;

    QVector<QVector<int> > redErrorData;
    QVector<QVector<int> > greenErrorData;
    QVector<QVector<int> > blueErrorData;

    Function* redPredictionFunction;
    Function* greenPredictionFunction;
    Function* bluePredictionFunction;

};
#endif // UTIL_H
