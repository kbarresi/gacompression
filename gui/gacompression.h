#ifndef GACOMPRESSION_H
#define GACOMPRESSION_H

#include "../util/util.h"

#include <QtWidgets/QMainWindow>
#include "ui_gacompression.h"

class Population;
class GACompression : public QMainWindow
{
    Q_OBJECT

public:
    GACompression(QWidget *parent = 0);
    ~GACompression();

public slots:
    void updateInputImage(QImage image);
    void updateInputImage();
    void updateErrorImages(QImage red, QImage green, QImage blue);
    void updateErrorImages();
    void updateDecompressedImage(QImage image);
    void updateDecompressedImage();

private slots:
    void loadButtonPressed();
    void startButtonPressed();
    void stopButtonPressed();
    void saveButtonPressed();
    void decompressButtonPressed();

    void applyPredictionFunction(Function* predictor);

private:
    Ui::GACompressionClass ui;


    //////////////////////////////////////
    // GUI components/functions
    /////////////////////////////////////
    CompressedImage* m_compressedImage;

    QImage m_workImage; //The full sized input image, on which everything is worked on

    //These are scaled down for display
    QImage m_inputImage;
    QImage m_redErrorImage;
    QImage m_greenErrorImage;
    QImage m_blueErrorImage;
    QImage m_decompressedImage;


    void placeAndCenter(QGraphicsPixmapItem* item, QGraphicsScene* scene);

    const static int BORDER_SIZE;
    ///////////////////////////////////////
    ///////////////////////////////////////


    ///////////////////////////////////////
    // Backend evolution interfaces
    ///////////////////////////////////////
    Population* m_redPopulation;
    Population* m_greenPopulation;
    Population* m_bluePopulation;


};

#endif // GACOMPRESSION_H
