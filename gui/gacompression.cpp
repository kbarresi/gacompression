#include "gacompression.h"

#include "../algorithm/errorimagefactory.h"
#include "../algorithm/population.h"
#include "../algorithm/program/function.h"

#include <QGraphicsView>
#include <QFileDialog>
#include <QDebug>

#include <QImage>
#include <QGraphicsItem>
#include <QProcess>

const int GACompression::BORDER_SIZE = 5;

GACompression::GACompression(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    ui.inputImageView->setScene(new QGraphicsScene(ui.inputImageView));

    ui.redErrorImageView->setScene(new QGraphicsScene(ui.redErrorImageView));
    ui.greenErrorImageView->setScene(new QGraphicsScene(ui.greenErrorImageView));
    ui.blueErrorImageView->setScene(new QGraphicsScene(ui.blueErrorImageView));

    ui.decompressedImageView->setScene(new QGraphicsScene(ui.decompressedImageView));

    connect(ui.loadButton, SIGNAL(pressed()), SLOT(loadButtonPressed()));
    connect(ui.startButton, SIGNAL(pressed()), SLOT(startButtonPressed()));
    connect(ui.stopButton, SIGNAL(pressed()), SLOT(stopButtonPressed()));
    connect(ui.saveButton, SIGNAL(pressed()), SLOT(saveButtonPressed()));
    connect(ui.decompressButton, SIGNAL(pressed()), SLOT(decompressButtonPressed()));

    ui.saveButton->setCheckable(false);

    m_compressedImage = 0;
    m_redPopulation = 0;
    m_greenPopulation = 0;
    m_bluePopulation = 0;
}

GACompression::~GACompression()
{
    if (m_redPopulation)
        delete m_redPopulation;
    if (m_greenPopulation)
        delete m_greenPopulation;
    if (m_bluePopulation)
        delete m_bluePopulation;
}

void GACompression::updateInputImage() {
    ui.inputImageView->scene()->clear();

    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QPixmap::fromImage(m_inputImage));
    placeAndCenter(item, ui.inputImageView->scene());

    if (m_redPopulation) {
        delete m_redPopulation;
        m_redPopulation = 0;
    }
    if (m_greenPopulation) {
        delete m_greenPopulation;
        m_greenPopulation = 0;
    }
    if (m_bluePopulation) {
        delete m_bluePopulation;
        m_bluePopulation = 0;
    }
    m_redPopulation = new Population(m_workImage, qRed); //create a population of individuals aimed at getting a prediction function for the image
    m_greenPopulation = new Population(m_workImage, qGreen);
    m_bluePopulation = new Population(m_workImage, qBlue);

    connect(m_redPopulation, SIGNAL(finished(Function*)), SLOT(applyPredictionFunction(Function*)));   //apply the predictor when finished
    connect(m_greenPopulation, SIGNAL(finished(Function*)), SLOT(applyPredictionFunction(Function*)));
    connect(m_bluePopulation, SIGNAL(finished(Function*)), SLOT(applyPredictionFunction(Function*)));

}
void GACompression::updateInputImage(QImage image) {
    m_inputImage = image;
    updateInputImage();
}
void GACompression::updateErrorImages() {
    ui.redErrorImageView->scene()->clear();
    ui.greenErrorImageView->scene()->clear();
    ui.blueErrorImageView->scene()->clear();

    QGraphicsPixmapItem *redItem = new QGraphicsPixmapItem(QPixmap::fromImage(m_redErrorImage));
    QGraphicsPixmapItem *greenItem = new QGraphicsPixmapItem(QPixmap::fromImage(m_greenErrorImage));
    QGraphicsPixmapItem *blueItem = new QGraphicsPixmapItem(QPixmap::fromImage(m_blueErrorImage));


    placeAndCenter(redItem, ui.redErrorImageView->scene());
    placeAndCenter(greenItem, ui.greenErrorImageView->scene());
    placeAndCenter(blueItem, ui.blueErrorImageView->scene());
}
void GACompression::updateErrorImages(QImage red, QImage green, QImage blue) {
    m_redErrorImage = red;
    m_greenErrorImage = green;
    m_blueErrorImage = blue;

    updateErrorImages();
}
void GACompression::updateDecompressedImage() {
    ui.decompressedImageView->scene()->clear();

    QGraphicsPixmapItem *item = new QGraphicsPixmapItem(QPixmap::fromImage(m_decompressedImage));
    placeAndCenter(item, ui.decompressedImageView->scene());
}
void GACompression::updateDecompressedImage(QImage image) {
    m_decompressedImage = image;
    updateDecompressedImage();
}

void GACompression::applyPredictionFunction(Function *predictor) {
    if (m_inputImage.isNull()) {
        qDebug() << "ERROR*** No input image given";
        return;
    }
    Population *pop = static_cast<Population*>(sender());

    if (pop == m_redPopulation)
        qDebug() << "\tReceived RED prediction model...applying";
    else if (pop == m_greenPopulation)
        qDebug() << "\tReceived GREEN prediction model...applying";
    else if (pop == m_bluePopulation)
        qDebug() << "\tReceived BLUE prediction model...applying";




    ErrorImageFactory *errorFactory = new ErrorImageFactory(m_workImage, predictor, predictor, predictor);

    QVector<QVector<int> > errorData;
    if (pop == m_redPopulation)
        errorData = errorFactory->redImage();
    else if (pop == m_greenPopulation)
        errorData = errorFactory->greenImage();
    else if (pop == m_bluePopulation)
        errorData = errorFactory->blueImage();

    QImage errorImage = Util::vector2Image(errorData);


    if (!m_compressedImage) {
        m_compressedImage = new CompressedImage();
    }

    if (pop == m_redPopulation) {
        m_compressedImage->redPredictionFunction = predictor;
        m_compressedImage->redErrorImage = errorImage;
        m_compressedImage->redErrorData = errorData;
        m_redErrorImage = m_compressedImage->redErrorImage;
    } else if (pop == m_greenPopulation) {
        m_compressedImage->greenPredictionFunction = predictor;
        m_compressedImage->greenErrorImage = errorImage;
        m_compressedImage->greenErrorData = errorData;
        m_greenErrorImage = m_compressedImage->greenErrorImage;
    } else if (pop == m_bluePopulation) {
        m_compressedImage->bluePredictionFunction = predictor;
        m_compressedImage->blueErrorImage = errorImage;
        m_compressedImage->blueErrorData = errorData;
        m_blueErrorImage = m_compressedImage->blueErrorImage;
    }

    updateErrorImages();

    ui.saveButton->setCheckable(true);
}

void GACompression::loadButtonPressed() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Select Image File"), "", tr("Image Files(*.png *.bmp *.jpg *.jpeg)"));
    QFile file(fileName);
    if (!file.exists()) {
        qDebug() << "No file given";
        file.close();
        return;
    }
    file.close();

    qDebug() << "Opening: " << fileName;

    QImage image(fileName);
    image = image.convertToFormat(QImage::Format_RGB32);

    if (image.isNull()) {
        qDebug() << "\t***Error*** Couldn't load image file";
        return;
    } else
        qDebug() << "\t...success";

    m_workImage = image;    //keep a copy of the original image size for working on...
    m_inputImage = image.scaled(ui.inputImageView->width() - BORDER_SIZE,    //and a shrunk size for display
                                ui.inputImageView->height() - BORDER_SIZE,
                                Qt::KeepAspectRatio, Qt::SmoothTransformation);

    updateInputImage();    
}
void GACompression::startButtonPressed() {
    qDebug() << "Starting GACompression...";
    if (m_inputImage.isNull()) {
        qDebug() << "\t...failed\tNo input image";
        return;
    }

    //Try running the genetic algorithm....
    m_redPopulation->run();
    m_greenPopulation->run();
    m_bluePopulation->run();

}
void GACompression::stopButtonPressed() {
    ui.inputImageView->scene()->clear();
    ui.redErrorImageView->scene()->clear();
    ui.greenErrorImageView->scene()->clear();
    ui.blueErrorImageView->scene()->clear();
    ui.decompressedImageView->scene()->clear();

    if (m_compressedImage) {
        delete m_compressedImage;
        m_compressedImage = 0;
    }

    ui.saveButton->setCheckable(false);
}
void GACompression::saveButtonPressed() {
    if (m_redErrorImage.isNull() ||
        m_greenErrorImage.isNull() ||
        m_blueErrorImage.isNull() ||
        !m_compressedImage)
        return;

    //Create the file saver, which will take care of parsing+writing
    FileSaver *code = new FileSaver();
    code->save(m_compressedImage);
}
void GACompression::decompressButtonPressed() {
    stopButtonPressed();    //stop anything that's currently going on...

    /* FILE STRUCTURE
     *
     * Header (2 bytes);
     * Separator (4 bytes);
     * Image width (4 bytes);
     * Image height (4 bytes);
     * Red Prediction Model Length (8 bytes);
     * Red Prediction Model
     * Green Prediction Model Length (8 bytes);
     * Green Prediction Model
     * Blue Prediction Model Length (8 bytes)
     * Blue Prediction Model
     * Separator (4 bytes);
     * Error Image Red Length (8 bytes);
     * Error Image Red(??? bytes);
     * Error Image Green Length (8 bytes);
     * Error Image Green(??? bytes);
     * Error Image Blue Length (8 bytes);
     * Error Image Blue(??? bytes)
     * Separator (4 bytes);
     * Red Border Pixel Length (8 bytes);
     * Red Border Pixels (??? bytes, <= [width + height - 1]);
     * Green Border Pixel Length (8 bytes);
     * Green Border Pixels (??? bytes, <= [width + height - 1]);
     * Blue Border Pixel Length (8 bytes);
     * Blue Border Pixels (??? bytes, <= [width + height - 1]);
     * Separator (4 bytes);
     * Separator (4 bytes);
     */

    qDebug() << "Reading compressed EPP image...";
    //Let the user select which *.epp file to read
    QString eppFilename = QFileDialog::getOpenFileName(this, tr("Select Image File"), "", tr("Image Files(*.epp)"));
    QFile eppFile(eppFilename);
    if (!eppFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open EPP file: " << eppFilename;
        eppFile.close();
        return;
    }

    //Read everything into a char arrays for processing...

    //Make sure we have a valid header
    char* header = (char*) malloc(sizeof(char*) * HEADER_LENGTH);
    eppFile.read(header, HEADER_LENGTH);

    if (header[0] != (char)0xAB || header[1] != (char)0xCD) {
        qDebug("Bad header: 0x%x%x", header[0], header[1]);
        free(header);
        return;
    }

    //Grab the separator
    char* separator = (char*) malloc(sizeof(char*) * SEPARATOR_LENGTH);
    eppFile.read(separator, SEPARATOR_LENGTH);

    //The width of the image
    char* widthBytes = (char*) malloc(sizeof(char*) * WIDTH_LENGTH);
    eppFile.read(widthBytes, WIDTH_LENGTH);
    qint32 width = (qint32) atol(widthBytes);
    free(widthBytes);

    //The height of the image
    char* heightBytes = (char*) malloc(sizeof(char*) * HEIGHT_LENGTH);
    eppFile.read(heightBytes, HEIGHT_LENGTH);
    qint32 height = (qint32) atol(heightBytes);
    free(heightBytes);

    //Get the lenth of each channel's prediction model, and then read the predictor model
    //and save it as a string
    char* redPredictionLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(redPredictionLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 redPredictionLength = (qint32) atol(redPredictionLengthBytes);
    free(redPredictionLengthBytes);

    char* redPredictorBytes = (char*) malloc(sizeof(char*) * redPredictionLength);
    eppFile.read(redPredictorBytes, redPredictionLength);
    QString redPredictor = QString::fromUtf8(redPredictorBytes, redPredictionLength);
    free(redPredictorBytes);

    char* greenPredictionLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(greenPredictionLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 greenPredictionLength = (qint32) atol(greenPredictionLengthBytes);
    free(greenPredictionLengthBytes);

    char* greenPredictorBytes = (char*) malloc(sizeof(char*) * greenPredictionLength);
    eppFile.read(greenPredictorBytes, greenPredictionLength);
    QString greenPredictor = QString::fromUtf8(greenPredictorBytes, greenPredictionLength);
    free(greenPredictorBytes);

    char* bluePredictionLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(bluePredictionLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 bluePredictionLength = (qint32) atol(bluePredictionLengthBytes);
    free(bluePredictionLengthBytes);

    char* bluePredictorBytes = (char*) malloc(sizeof(char*) * bluePredictionLength);
    eppFile.read(bluePredictorBytes, bluePredictionLength);
    QString bluePredictor = QString::fromUtf8(bluePredictorBytes, bluePredictionLength);
    free(bluePredictorBytes);

    char *separatorTest = (char*) malloc(sizeof(char*) * SEPARATOR_LENGTH);
    eppFile.read(separatorTest, SEPARATOR_LENGTH);
    if (!(separatorTest[0] == (char)0xDE &&
          separatorTest[1] == (char)0xAD &&
          separatorTest[2] == (char)0xBE &&
          separatorTest[3] == (char)0xEF)) {
        qDebug() << "\tseparator not found after prediction models...";
        qDebug("Found: %x%x%x%x", separatorTest[0], separatorTest[1], separatorTest[2], separatorTest[3]);
        return;
    }
    free(separatorTest); //that was just a check, don't need it anymore



    //Now go grab each channel's error image
    char* redErrorLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(redErrorLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 redErrorLength = (qint32) atol(redErrorLengthBytes);
    free(redErrorLengthBytes);

    char* redCompressedError = (char*) malloc(sizeof(char*) * redErrorLength);
    eppFile.read(redCompressedError, redErrorLength);


    //Green channel
    char* greenErrorLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(greenErrorLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 greenErrorLength = (qint32) atol(greenErrorLengthBytes);
    free(greenErrorLengthBytes);

    char* greenCompressedError = (char*) malloc(sizeof(char*) * greenErrorLength);
    eppFile.read(greenCompressedError, greenErrorLength);


    //and finally, blue
    char* blueErrorLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(blueErrorLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 blueErrorLength = (qint32) atol(blueErrorLengthBytes);
    free(blueErrorLengthBytes);

    char* blueCompressedError = (char*) malloc(sizeof(char*) * blueErrorLength);
    eppFile.read(blueCompressedError, blueErrorLength);


    //Ok, just check that there's a separator and we'll move on
    separatorTest = (char*) malloc(sizeof(char*) * SEPARATOR_LENGTH);
    eppFile.read(separatorTest, SEPARATOR_LENGTH);
    if (!(separatorTest[0] == (char)0xDE &&
          separatorTest[1] == (char)0xAD &&
          separatorTest[2] == (char)0xBE &&
          separatorTest[3] == (char)0xEF)) {
        qDebug() << "\tseparator not found after error images...";
        qDebug("Found: %x%x%x%x", separatorTest[0], separatorTest[1], separatorTest[2], separatorTest[3]);
        return;
    }
    free(separatorTest); //that was just a check, don't need it anymore


    //Get each channel's border pixels

    char* redBorderLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(redBorderLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 redCompressedBorderLength = (qint32) atol(redBorderLengthBytes);
    char* redCompressedBorder = (char*) malloc(sizeof(char*) * redCompressedBorderLength);
    eppFile.read(redCompressedBorder, redCompressedBorderLength);
    free(redBorderLengthBytes);


    char* greenBorderLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(greenBorderLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 greenCompressedBorderLength = (qint32) atol(greenBorderLengthBytes);
    char* greenCompressedBorder = (char*) malloc(sizeof(char*) * greenCompressedBorderLength);
    eppFile.read(greenCompressedBorder, greenCompressedBorderLength);
    free(greenBorderLengthBytes);


    char* blueBorderLengthBytes = (char*) malloc(sizeof(char*) * ERROR_IMAGE_LENGTH);
    eppFile.read(blueBorderLengthBytes, ERROR_IMAGE_LENGTH);
    qint32 blueCompressedBorderLength = (qint32) atol(blueBorderLengthBytes);
    char* blueCompressedBorder = (char*) malloc(sizeof(char*) * blueCompressedBorderLength);
    eppFile.read(blueCompressedBorder, blueCompressedBorderLength);
    free(blueBorderLengthBytes);

    eppFile.close();

    //Ok so at this point, we have all the data we need to reconstruct the image. However, we need to get the original bytes of the error
    //image, since they're encoded. Also, the border pixels need to be decompressed. We'll start with the error image first
    QString tempFilename1 = "decompress_tmp1";
    QString tempFilename2 = "decompress_tmp2";
    QFile compressedErrorImageFile(tempFilename1);
    if (!compressedErrorImageFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to create a new file for temp work";
        return;
    }

    compressedErrorImageFile.write(redCompressedError, redErrorLength);
    compressedErrorImageFile.close();

    //Now unencode it with the arithmetic encoder utility.
    //Set up the proces call to the arithmetic encoder
#ifdef Q_OS_WIN
    QString decodeProcess = QString("../GACompression/resources/ac.exe");
#endif
#ifdef Q_OS_LINUX
    QString decodeProcess = QString("../GACompression/resources/ac");
#endif

    QStringList parameters;
    parameters << tempFilename1 << tempFilename2;   //ac.exe writes the decompressed data to the second file

    QProcess *proc = new QProcess(0);
    proc->start(decodeProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;
    //so now the second file contains the error image data!


    //Open the file containing the decompressed data
    QFile decompressedErrorImageFile(tempFilename2);
    if (!decompressedErrorImageFile.open(QIODevice::ReadWrite)) {
        qDebug() << "Unable to open the decompressed red error image file";
        return;
    }
    //and read it into a char array for us to use
    int redDecompressedErrorLength = decompressedErrorImageFile.bytesAvailable();
    char* redDecompressedError = (char*) malloc(sizeof(char*) * redDecompressedErrorLength);
    decompressedErrorImageFile.read(redDecompressedError, redDecompressedErrorLength);
    decompressedErrorImageFile.remove();
    decompressedErrorImageFile.close();

    //
    //Repeat for the green channel
    //
    if (!compressedErrorImageFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to create open file for green channel";
        return;
    }
    compressedErrorImageFile.write(greenCompressedError, greenErrorLength);
    compressedErrorImageFile.close();
    proc = new QProcess(0);
    proc->start(decodeProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;
    if (!decompressedErrorImageFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open the decompressed green error image file";
        return;
    }
    int greenDecompressedErrorLength = decompressedErrorImageFile.bytesAvailable();
    char* greenDecompressedError = (char*) malloc(sizeof(char*) * greenDecompressedErrorLength);
    decompressedErrorImageFile.read(greenDecompressedError, greenDecompressedErrorLength);
    decompressedErrorImageFile.remove();
    decompressedErrorImageFile.close();


    //
    //And for the blue channel
    //
    if (!compressedErrorImageFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to create open file for green channel";
        return;
    }
    compressedErrorImageFile.write(blueCompressedError, blueErrorLength);
    compressedErrorImageFile.close();
    proc = new QProcess(0);
    proc->start(decodeProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;
    if (!decompressedErrorImageFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Unable to open the decompressed green error image file";
        return;
    }
    int blueDecompressedErrorLength = decompressedErrorImageFile.bytesAvailable();
    char* blueDecompressedError = (char*) malloc(sizeof(char*) * blueDecompressedErrorLength);
    decompressedErrorImageFile.read(blueDecompressedError, blueDecompressedErrorLength);
    decompressedErrorImageFile.remove();
    decompressedErrorImageFile.close();


    //So now we have all the error image data!
    //Time to decompress the border pixel channels


    //Start by writing red data to the file
    QFile borderPixelFile(tempFilename1 + ".gz");
    if (!borderPixelFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to open compressed pixel border file";
        return;
    }
    borderPixelFile.write(redCompressedBorder, redCompressedBorderLength);
    borderPixelFile.close();

    //Now unzip the border image pixels
#ifdef Q_OS_WIN
    QString unzipProcess = QString("../GACompression/resources/gzip.exe");
#endif
#ifdef Q_OS_LINUX
    QString unzipProcess = QString("../GACompression/resources/gzip");
#endif
    parameters.clear();
    parameters << "-d" << "-f" << (tempFilename1 + ".gz");
    proc = new QProcess(0);
    proc->start(unzipProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;

    //Our file now contains the decompressed data
    borderPixelFile.setFileName(tempFilename1);
    if (!borderPixelFile.open(QIODevice::ReadWrite)) {
        qDebug() << "Unable to open decompressed pixel border file";
        return;
    }
    //read the decompressed border pixels
    int redDecompressedBorderLength = borderPixelFile.bytesAvailable();
    qDebug() << "\t...red pixel border length: " << redDecompressedBorderLength;
    char* redDecompressedBorder = (char*) malloc(sizeof(char*) * redDecompressedBorderLength);
    borderPixelFile.read(redDecompressedBorder, redDecompressedBorderLength);
    borderPixelFile.remove();
    borderPixelFile.close();


    //Repeat for green....
    borderPixelFile.setFileName(tempFilename1 + ".gz");
    if (!borderPixelFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to open compressed pixel border file";
        return;
    }
    borderPixelFile.write(greenCompressedBorder, greenCompressedBorderLength);
    borderPixelFile.close();
    proc = new QProcess(0);
    proc->start(unzipProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;

    //Our file now contains the decompressed data
    borderPixelFile.setFileName(tempFilename1);
    if (!borderPixelFile.open(QIODevice::ReadWrite)) {
        qDebug() << "Unable to open decompressed pixel border file";
        return;
    }
    //read the decompressed border pixels
    int greenDecompressedBorderLength = borderPixelFile.bytesAvailable();
    qDebug() << "\t...green pixel border length: " << greenDecompressedBorderLength;
    char* greenDecompressedBorder = (char*) malloc(sizeof(char*) * greenDecompressedBorderLength);
    borderPixelFile.read(greenDecompressedBorder, greenDecompressedBorderLength);
    borderPixelFile.remove();
    borderPixelFile.close();



    //And finally for the blue channel:
    borderPixelFile.setFileName(tempFilename1 + ".gz");
    if (!borderPixelFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
        qDebug() << "Unable to open compressed pixel border file";
        return;
    }
    borderPixelFile.write(blueCompressedBorder, blueCompressedBorderLength);
    borderPixelFile.close();
    proc = new QProcess(0);
    proc->start(unzipProcess, parameters);
    proc->waitForFinished();
    proc->deleteLater();
    proc = 0;

    //Our file now contains the decompressed data
    borderPixelFile.setFileName(tempFilename1);
    if (!borderPixelFile.open(QIODevice::ReadWrite)) {
        qDebug() << "Unable to open decompressed pixel border file";
        return;
    }
    //read the decompressed border pixels
    int blueDecompressedBorderLength = borderPixelFile.bytesAvailable();
    qDebug() << "\t...blue pixel border length: " << blueDecompressedBorderLength;
    char* blueDecompressedBorder = (char*) malloc(sizeof(char*) * blueDecompressedBorderLength);
    borderPixelFile.read(blueDecompressedBorder, blueDecompressedBorderLength);
    borderPixelFile.remove();
    borderPixelFile.close();


    //So as of now, we have the prediction models as a string, the error images as a char array, and the border pixels in a char array.
    //Now it's time to create a CompressedImage structure so we can rebuild the image using that data.

    CompressedImage* compressedImage = new CompressedImage();

    //place the red error image + border pixels into a matrix-like object so it can easily be turned into an image later
    QVector<QVector<int> > redVectorArray;
    QVector<int> currentRow;
    int effectiveHeight = height - 1;
    for (int i = 0; i < redDecompressedErrorLength + redDecompressedBorderLength; i++) {
        int pixel = 0;
        if (i < width)
            pixel  = static_cast<int>((uchar)redDecompressedBorder[ (height - 1) + i]);
        else if (i % width == 0) {
            redVectorArray.append(currentRow);
            currentRow.clear();
            pixel = static_cast<int>((uchar)redDecompressedBorder[effectiveHeight - (i / width)]);
        } else
            pixel = static_cast<int>((uchar)redDecompressedError[i - width - (i / width)]);
        currentRow.append(pixel);
    }
    redVectorArray.append(currentRow);
    compressedImage->redErrorData = redVectorArray;
    compressedImage->redErrorImage = Util::vector2Image(redVectorArray);


    //Repeat for green channel:
    QVector<QVector<int> > greenVectorArray;
    currentRow.clear();
    for (int i = 0; i < greenDecompressedErrorLength + greenDecompressedBorderLength; i++) {
        int pixel = 0;
        if (i < width)
            pixel  = static_cast<int>((uchar)greenDecompressedBorder[ (height - 1) + i]);
        else if (i % width == 0) {
            greenVectorArray.append(currentRow);
            currentRow.clear();
            pixel = static_cast<int>((uchar)greenDecompressedBorder[effectiveHeight - (i / width)]);
        } else
            pixel = static_cast<int>((uchar)greenDecompressedError[i - width - (i / width)]);
        currentRow.append(pixel);
    }
    greenVectorArray.append(currentRow);
    compressedImage->greenErrorData = greenVectorArray;
    compressedImage->greenErrorImage = Util::vector2Image(greenVectorArray);

    //And for the blue channel:
    QVector<QVector<int> > blueVectorArray;
    currentRow.clear();
    for (int i = 0; i < blueDecompressedErrorLength + blueDecompressedBorderLength; i++) {
        int pixel = 0;
        if (i < width)
            pixel  = static_cast<int>((uchar)blueDecompressedBorder[ (height - 1) + i]);
        else if (i % width == 0) {
            blueVectorArray.append(currentRow);
            currentRow.clear();
            pixel = static_cast<int>((uchar)blueDecompressedBorder[effectiveHeight - (i / width)]);
        } else
            pixel = static_cast<int>((uchar)blueDecompressedError[i - width - (i / width)]);
        currentRow.append(pixel);
    }
    blueVectorArray.append(currentRow);
    compressedImage->blueErrorData = blueVectorArray;
    compressedImage->blueErrorImage = Util::vector2Image(blueVectorArray);

    //Now we need to reconstruct the prediction model tree
    Function *redModel = new Function(Function::Add, 0);    //just leave these as Add, it'll fix itself during the deserialization
    Function *greenModel = new Function(Function::Add, 0);
    Function *blueModel = new Function(Function::Add, 0);

    redModel->deserialize(redPredictor);
    greenModel->deserialize(greenPredictor);
    blueModel->deserialize(bluePredictor);

    compressedImage->redPredictionFunction = redModel;
    compressedImage->greenPredictionFunction = greenModel;
    compressedImage->bluePredictionFunction = blueModel;


    for (int i = 0; i < 10; i++) {
        qDebug("%d %d %d", (uchar)redDecompressedBorder[i], (uchar)greenDecompressedBorder[i], (uchar)blueDecompressedBorder[i]);
    }
    qDebug() << "\n\n";

    int h = compressedImage->redErrorData.size() - 1;
    for (int i = 0; i < 10; i++) {
        qDebug("%d %d %d", compressedImage->redErrorData.at(h - i).at(0), compressedImage->greenErrorData.at(h - i).at(0), compressedImage->blueErrorData.at(h - i).at(0));
    }


    QImage reconstructedImage = Util::reconstructImage(compressedImage);

    m_decompressedImage = reconstructedImage;
    updateDecompressedImage();

    free(header);
    free(separator);
    free(redDecompressedBorder);
    free(redCompressedBorder);
    free(redCompressedError);
    free(redDecompressedError);

    free(greenDecompressedBorder);
    free(greenCompressedBorder);
    free(greenCompressedError);
    free(greenDecompressedError);

    free(blueDecompressedBorder);
    free(blueCompressedBorder);
    free(blueCompressedError);
    free(blueDecompressedError);
}

void GACompression::placeAndCenter(QGraphicsPixmapItem *item, QGraphicsScene* scene) {
    scene->addItem(item);

    QPointF sceneOrigin = scene->views().first()->mapToScene(QPoint(0, 0));

    item->setPos(sceneOrigin.x(), sceneOrigin.y());
}
